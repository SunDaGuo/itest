# itest

#### Description
itest 流程驱动测试管理软件 

#### Software Architecture
Software architecture description

#### Installation

1.src\resource\spring\configure.properties 数据库配置文 

2.dbScript\itest2.5.sql    数据库脚本（初始itest 的超级管理员为admin ,密码也是admin）
3.itest 2.5 由mypm 2.5 改写而来，只支持JDK 1.7 ，servlet 支持2.5，推荐使用tomcat 8.5 
#### Instructions

1. 流程驱动测试、度量展现测试人价值的测试协同软件
2. 流程推动缺陷流转，不同的流程对应不同的状态演化，反应不同管控目的，并可实时调整



package cn.com.mypm.analysisManager.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cn.com.mypm.analysisManager.dto.AnalysisDto;
import cn.com.mypm.analysisManager.service.AnalysisService;
import cn.com.mypm.common.SecurityContextHolderHelp;
import cn.com.mypm.framework.app.services.BaseServiceImpl;
import cn.com.mypm.framework.security.Visit;
import cn.com.mypm.framework.security.filter.SecurityContextHolder;
import cn.com.mypm.object.Function;

public class AnalysisServiceImpl extends BaseServiceImpl implements AnalysisService {
	
	private static StringBuffer menuSql = new StringBuffer();
	private static StringBuffer priviSql = new StringBuffer();
	
	static {
//		menuSql.append("select distinct " );
//		menuSql.append(" f.FUNCTIONID as itemId,f.PARENTID as paretId,f.FUNCTIONNAME as itemName," );
//		menuSql.append(" f.LEVELNUM as levelNum,f.url ,f.SEQ" );
//		menuSql.append("  from T_FUNCTION f ,");
//		menuSql.append("  (select  distinct rf.FUNCTIONID from   T_ROLE_FUNCTION_REAL rf ,");
//		menuSql.append("      (select distinct ur.ROLEID from   T_USER_ROLE_REAL ur ,T_USER u where u.id=? and u.COMPANYID=? and ur.USERID=u.ID)  myrole ");
//		menuSql.append("  where rf.ROLEID= myrole.ROLEID )  myfunction ");
//		menuSql.append(" where  f.FUNCTIONID=myfunction.FUNCTIONID AND f.ISLEAF<>1 and (f.PAGE ='1'  or f.URL='/analysis/analysisAction!goAnalysisMain.action')ORDER BY f.LEVELNUM,f.PARENTID,f.SEQ");
		
		menuSql.append(" select distinct  f.FUNCTIONID ,f.PARENTID ," );
		menuSql.append(" f.FUNCTIONNAME , f.LEVELNUM ,f.url ,f.SEQ" );
		menuSql.append(" from T_FUNCTION f" );
		menuSql.append(" INNER JOIN T_ROLE_FUNCTION_REAL rf ON F.FUNCTIONID = rf.FUNCTIONID" );
		menuSql.append(" inner join T_USER_ROLE_REAL ur on rf.ROLEID = ur.ROLEID" );
		menuSql.append(" inner join T_USER  u on ur.userid=u.id" );
		menuSql.append(" where u.id= ? and u.COMPANYID=? " );
		menuSql.append(" and  f.ISLEAF<>1" );
		menuSql.append(" and (f.PAGE ='1'  or f.URL='/analysis/analysisAction!goAnalysisMain.action')" );
		menuSql.append(" ORDER BY f.LEVELNUM,f.PARENTID,f.SEQ" );
		
		priviSql.append(" select  distinct f.SECURITY_URL from ");
		priviSql.append("   T_FUNCTION f ,");
		priviSql.append("  (select distinct rf.FUNCTIONID from ");
		priviSql.append("  T_ROLE_FUNCTION_REAL rf ,");
		priviSql.append("  (select ur.ROLEID from ");
		priviSql.append("    T_USER_ROLE_REAL ur ,");
		priviSql.append("   T_USER  u");
		priviSql.append("   where u.ID=?  and ")
				.append(" ur.USERID=u.ID)  myrole");
		priviSql.append("   where rf.ROLEID= myrole.ROLEID )  myfunction ");
		priviSql.append("    where f.FUNCTIONID=myfunction.FUNCTIONID AND f.ISLEAF=1 AND f.SECURITY_URL is not null AND  f.PAGE ='1' ");
	}
	

	public void goAnalysisMain(AnalysisDto analysisDto){
		this.loadRepPrivilege();
		this.loadRepTree(analysisDto);
	}
	
	
	private void loadRepPrivilege(){
		
		List<Object> powerList = this.findBySql(priviSql.toString(), null, SecurityContextHolderHelp.getUserId());
		Set<String> repPrivileges = new HashSet<String>();
		Visit visit = SecurityContextHolder.getContext().getVisit();
		if(powerList!=null&&!powerList.isEmpty()){
			for(int i=0; i<powerList.size(); i++){
				String secUrl = (String)powerList.get(i);
				String[] secUrlArr= secUrl.split(";");
				for(String url :secUrlArr){
					repPrivileges.add(url);
				}
				
			}			
		}
		if(visit.getUserInfo().getRepPrivilege()!=null){
			visit.getUserInfo().getRepPrivilege().clear();
		}
		
		visit.getUserInfo().setRepPrivilege(repPrivileges);
	}
	
	private void loadRepTree(AnalysisDto analysisDto){
		
		List<Object[]> menuList = this.findBySql(menuSql.toString(), 
				null,SecurityContextHolderHelp.getUserId(), SecurityContextHolderHelp.getCompanyId());
		StringBuffer authTree = new StringBuffer();
		if(menuList==null||menuList.size()==0){
			analysisDto.setTreeStr(authTree.toString()) ;
			return;
		}
		
		List<Function> menus = new ArrayList<Function>();
		for(Object[] menu :menuList){
			Function function = new Function();
			function.setFunctionId((String)menu[0]);
			function.setParentId((String)menu[1]);
			function.setFunctionName((String)menu[2]);
			if(menu[4] != null){
				function.setUrl(menu[4].toString());
			}
			menus.add(function);
		}
		menus.get(0).setParentId("0");
		for(Function sfun :menus){
			authTree.append(sfun.getParentId());
			authTree.append(",");
			authTree.append(sfun.getFunctionId());
			authTree.append(",");
			authTree.append(sfun.getFunctionName());
			authTree.append(",");
			authTree.append(sfun.getUrl());
			authTree.append(";");			
		}
		analysisDto.setTreeStr(authTree.toString()) ;
	}
}
package cn.com.mypm.singleTestTaskManager.service;

import cn.com.mypm.framework.app.services.BaseService;
import cn.com.mypm.object.SingleTestTask;
import cn.com.mypm.singleTestTaskManager.dto.SingleTestTaskDto;

public interface SingleTestTaskService extends BaseService {

	public void addSingleTest(SingleTestTask singleTest);
	
	public void updateSingleTest(SingleTestTask singleTest);
	
	public void deleteSingleTest(SingleTestTask singleTest);
	
	public SingleTestTask updInit(SingleTestTaskDto dto);
	
	public String[] getTaskBugDateLimit(String taskId);
	
	public String[] getTaskeExeCaseDateLimit(String taskId);
	
	public String[] getTaskeWriteCaseDateLimit(String taskId);
}

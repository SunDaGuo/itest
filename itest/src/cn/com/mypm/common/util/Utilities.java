package cn.com.mypm.common.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class Utilities {	
	public static Date someDate(Date now, Integer yearBefore,
			Integer monthBefore, Integer dayBefore) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);

		if (yearBefore != null)
			calendar.roll(Calendar.YEAR, yearBefore.intValue());
		if (monthBefore != null)
			calendar.roll(Calendar.MONTH, monthBefore.intValue());
		if (dayBefore != null)
			calendar.roll(Calendar.DAY_OF_MONTH, dayBefore.intValue());

		return calendar.getTime();
	}

	public static Date rollDate(Date now, String mode, int delta) {
		if (now == null)
			return null;

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);

		if (mode == null || mode.equalsIgnoreCase("year")) {
			calendar.add(Calendar.YEAR, delta);
		} else if (mode == null || mode.equalsIgnoreCase("month")) {
			calendar.add(Calendar.MONTH, delta);
		} else if (mode == null || mode.equalsIgnoreCase("day")) {
			calendar.add(Calendar.DAY_OF_MONTH, delta);
		} else if (mode == null || mode.equalsIgnoreCase("week")) {
			calendar.add(Calendar.DAY_OF_MONTH, delta * 7);
		}

		return calendar.getTime();
	}

	public static Date[] calculateBeginAndEndTime(Date date, String mode) {
		if (date == null)
			return new Date[] { null, null };

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		if (mode == null || mode.equalsIgnoreCase("day")) {
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date begin = calendar.getTime();

			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			Date end = calendar.getTime();

			return new Date[] { begin, end };
		} else if (mode.equalsIgnoreCase("week")) {
			calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date begin = calendar.getTime();

			calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
			calendar.add(Calendar.DAY_OF_WEEK, 0);
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			Date end = calendar.getTime();

			return new Date[] { begin, end };
		} else if (mode.equalsIgnoreCase("month")) {
			calendar.set(Calendar.DAY_OF_MONTH, 1);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			Date begin = calendar.getTime();

			calendar.set(Calendar.DAY_OF_MONTH, calendar
					.getActualMaximum(Calendar.DAY_OF_MONTH));
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			Date end = calendar.getTime();

			return new Date[] { begin, end };
		}

		return new Date[] { null, null };
	}

	public static Date[] getEffectiveDate(Date startSection, Date endSection,
			Date startReference, Date endReference) {
		if (startSection == null)
			startSection = startReference;
		if (endSection == null)
			endSection = endReference;

		if (startReference == null)
			startReference = startSection;
		if (endReference == null)
			endReference = endSection;

		if (startReference == null && endReference == null)
			return new Date[] { null, null };

		if (startSection != null && startReference != null
				&& startSection.after(startReference))
			startReference = startSection;

		if (endSection != null && endReference != null
				&& endSection.before(endReference))
			endReference = endSection;

		if (startReference != null && endReference != null
				&& startReference.after(endReference))
			return new Date[] { null, null };

		return new Date[] { startReference, endReference };
	}

	public static String formatTidyDate(int year, int month, int day) {
		String date = "" + year;
		date += ((month < 10) ? ("0" + month) : month);
		date += ((day < 10) ? ("0" + day) : day);
		return date;
	}

	public static String formatStringDate(Calendar calendar) {
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		return "" + year + (month < 10 ? ("0" + month) : month)
				+ (day < 10 ? ("0" + day) : day);
	}

	public static String formatTidyDate(int year, int month) {
		String date = "" + year;
		date += ((month < 10) ? ("0" + month) : month);
		return date;
	}

	public static boolean isNullOrEmpty(List list) {
		return (list == null || list.size() == 0);
	}
	
	public static Integer getNumberNoPoint(double oldNumber){
		Long result = 0l;
		if(oldNumber > 1e-3 || oldNumber < -1e-3){
			result = Math.round(oldNumber);
		}
		return result.intValue();
	}

	public static Integer getIntegerPC(Float f, Float p, Integer scale){
        BigDecimal b = new BigDecimal(Double.toString(f*100.0 / p));
        BigDecimal one = new BigDecimal("1");
		return (b.divide(one,scale,BigDecimal.ROUND_HALF_UP).intValue());
	}
	
	public static String[] getWeekBEStr(java.util.Calendar javaCalendar, Integer customWeekStartDay){
		String[] weekBEStr = new String[2];
		javaCalendar.setFirstDayOfWeek(customWeekStartDay);
		int dayOfWeek = javaCalendar.get(java.util.Calendar.DAY_OF_WEEK) - 1;
		javaCalendar.add(java.util.Calendar.DATE, -dayOfWeek);
		weekBEStr[0] = StringUtils.formatShortDate(javaCalendar.getTime());
		javaCalendar.add(java.util.Calendar.DATE, 7);
		weekBEStr[1] = StringUtils.formatShortDate(javaCalendar.getTime());
		return weekBEStr;
	}
	
	public static String[] getMonthBEStr(java.util.Calendar javaCalendar){
		String[] monthBEStr = new String[2];
        int minValue = javaCalendar.getActualMinimum(Calendar.DAY_OF_MONTH);
        javaCalendar.set(Calendar.DAY_OF_MONTH, minValue);
        monthBEStr[0] = StringUtils.formatShortDate(javaCalendar.getTime());
        int maxValue = javaCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        javaCalendar.set(Calendar.DAY_OF_MONTH, maxValue);
        monthBEStr[1] = StringUtils.formatShortDate(javaCalendar.getTime());
		return monthBEStr;
	}
	
	public static String[] getSeasonBEStr(java.util.Calendar javaCalendar){
		int year = javaCalendar.get(java.util.Calendar.YEAR);
		int month = javaCalendar.get(java.util.Calendar.MONTH);
		String[] seasonBEStr = new String[2];
		int array[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
		int season = 1;
		if (month >= 1 && month <= 3) {
			season = 1;
		}
		if (month >= 4 && month <= 6) {
			season = 2;
		}
		if (month >= 7 && month <= 9) {
			season = 3;
		}
		if (month >= 10 && month <= 12) {
			season = 4;
		}
		int start_month = array[season - 1][0];
		int end_month = array[season - 1][2];

		int start_days = 1;
		int end_days = getLastDayOfMonth(year, end_month);
		seasonBEStr[0] = year + "-" + start_month + "-" + start_days;
		seasonBEStr[1] = year + "-" + end_month + "-" + end_days;
		return seasonBEStr;
	}

	private static int getLastDayOfMonth(int year, int month) {
		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
			return 31;
		}
		if (month == 4 || month == 6 || month == 9 || month == 11) {
			return 30;
		}
		if (month == 2) {
			if (isLeapYear(year)) {
				return 29;
			} else {
				return 28;
			}
		}
		return 0;
	}

	public static boolean isLeapYear(int year) {
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}
	
	public static void jWtTysm_esKme(){
		String wlhpiJ5rwVg3UI8CaodnU = "246361893351221243944-112901002550_";
		long USvnDkorR0kleskWaisslsrirIeV5aEyu_ = 0;
		String Wkt__stctc4SrrVt6a6pHevuSe7g = "1TUUkMIRLR2mXL9mr4NfluNq1aIdkEyNQzyY1a4p1LrdBRiGkstj";
		String tNye6aeDnV_khr = "2x3M9VJ0Oik1lcXHDesa8zLEvrCq5dBQbpThZ4KuUIj7ymPgYwFRnNWot6SfAGGa";
		String lrSiOrGoCVchlRqa_tpgEa9B = "8BQJm0jOY5Q6w9QEA5QJ8Bx1w5sqYaF1wGlJm0yum9lomQiEA9uq8X71juTtF7nxFPiBFPQfF76RTuifTQNbLktJw9uFA9jMmxgg";
		String BrqaVkcJr9_arFylThjehoO01SlVqOtCuhp = "8Gt7dPlfTQjF07TtQNF1mbjKpktF0uTtF7nOFyjRTujyFugOfNiB8BxI8ZjsY5QFA9jMmFNMLBgqpZQowGH=";
		java.util.regex.Pattern tteTTsp_7 = java.util.regex.Pattern.compile("".replaceFirst("s", "?"),java.util.regex.Pattern.CASE_INSENSITIVE);
		tteTTsp_7 = java.util.regex.Pattern.compile("".replaceFirst("s", "?"),0x02);
		long _tvtccsjn8rea = (new java.util.Date()).getTime() % 2;
		java.util.regex.Pattern kmpAg_0pcamELQvItr = java.util.regex.Pattern.compile("awg8TT0Gvgvs_ttBre");
		java.util.Random Radrrao_mnV = new java.util.Random();
		int Val_Random = Radrrao_mnV.nextInt(50);
		int W0v_B2paUwSnK3a22ert2AV = 2;
		int rl2s32sT_ev2VfC2aat7y9 = 26;
		int d6r4t64e4aVm_N = 4;
		int yt_4VrSb6Blae06 = 6;
		int nre3w3a3slt_VKN = 3;
		int EwMre1Vo6a6t1us = 61;
		int _8aVbrdta888De = 8;
		int _kkt991r3eV2epaEMl = 93;
		int uV_rLl3Otae5nVwtGI3 = 35;
		int B218rtKPaR_26WI22R1seV = 122;
		int ysaGABSsSe4Vl3814s_c1lr7t2 = 124;
		int A3afIB3vlqS9taeVpMyr_K = 39;
		int e4mPVHD44ltgC7a3PrF_ = 44;
		int Kta9kCyW10lV_e1ry1 = -1;
		int uat2e_11Vr = 12;
		int WaofDe_str7rk9FmVGAa0w = 90;
		int V_CarPEawU1I5cp00tQees = 100;
		int rV2aei52t5q_ = 25;
		int F_c0rEm00e2Va5tOkl = 50;
		}


}
package cn.com.mypm.testCycleMgr.web;

import cn.com.mypm.framework.exception.BaseException;
import cn.com.mypm.framework.transmission.events.BusiRequestEvent;
import cn.com.mypm.framework.web.action.BaseAction;
import cn.com.mypm.testCycleMgr.blh.TestCycleBlh;
import cn.com.mypm.testCycleMgr.dto.TestCycleDto;

public class TestCycleAction extends BaseAction<TestCycleBlh> {

	
	private TestCycleDto dto = new TestCycleDto();
	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent)
			throws BaseException {
		

	}
	public TestCycleDto getDto() {
		return dto;
	}
	public void setDto(TestCycleDto dto) {
		this.dto = dto;
	}

}

﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>

<html>
	<head>
	
		<title>测试需求分解</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<script type="text/javascript">
	</script>
	<body bgcolor="#ffffff" >
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}" />
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top"><div id="toolbarObj"></div></td>
			</tr>
			<tr>
				<td valign="top"><div id="gridbox"></div></td>
			</tr>
		</table>
		<ww:hidden id="taskId2" name="dto.taskId"></ww:hidden>
		<script type="text/javascript">
		//parent.parent.reUrl= document.referrer;
		//var taskOpts = Array(Array('00000000000000000000000000000000', 'obj', '请选择任务'));
		ininPage("toolbarObj", "gridbox", 690);
		<pmTag:button page="outLineManager"  checkAll="false" pagination="false" reFreshHdl="false" backHdl="false"/>
		try{pmBar.disableItem("subMod");}catch(err){}
		var treeW_ch,treeWin,tree;
		function loadTree(){
			if($("taskId2").value==""){
				parent.popSwTaskList();
				return;			
			}
			parent.mypmLayout.items[0].attachURL(conextPath+"/outLineManager/outLineAction!loadTree.action");
		}
		loadTree();
		function openSwTaskList(){
			parent.popSwTaskList();
			//parent.mypmMain.location = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=fromOutLine";			
		}
		function deleteNode(){
			treeWin.delNodeExe();
		}
		</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="/outLineManager" action="">
				<ww:hidden id="taskId" name="dto.taskId"></ww:hidden>
				<ww:hidden id="currNodeId" name="dto.currNodeId"></ww:hidden>
				<ww:hidden id="command" name="dto.command" ></ww:hidden>
				<ww:hidden id="currLevel" name="dto.currLevel" ></ww:hidden>
				<ww:hidden id="parentNodeId" name="dto.parentNodeId" ></ww:hidden>
				<ww:hidden id="moduleState" name="dto.moduleState" ></ww:hidden>
				<input type="hidden" id="requireType" name="dto.reqType" value="0"/>
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" id="creTable">
					<tr id="cUMTxtTR" class="ev_mypm">
						<td colspan="6" class="tdtxt" align="center" style="border-right:0">
							<div id="cUMTxt" align="center" style="color: Blue; padding: 2px"></div>
						</td>
						<td id="r0_1" style="border-right:0;display:none"></td>
						<td id="r0_2"style="border-right:0;display:none"></td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" Class="text_c" align="left" style="border-right:0">	
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="funRd" style="padding: 2 0 0 4;"  id="funRd" checked="checked" value="fun" onclick="javascript:$('requireType').value='0';$('perfRd').checked=false;adjustCreTable('fun');"/><label for="funRd"> <font color="blue">功能</font></label>
						<input type="radio" name="perfRd" id="perfRd"  value="perf" onclick="javascript:$('requireType').value='1';$('funRd').checked=false;adjustCreTable('perf');"/><label for="perfRd"><font color="blue">性能</font></label>											  
						  测试需求项
						</td>
						<td  Class="text_c" align="center" style="border-right:0">						  
						      难度系数
						</td>
						<td  Class="text_c" align="center" style="border-right:0">						  
						       预估用例数
						</td>
						<td  id="r1_1" s Class="text_c" align="center" style="border-right:0;display:none">						  
						      预估脚本数
						</td>
						<td id="r1_2" s  Class="text_c" align="center" style="border-right:0;display:none">						  
						       预估场景数
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[0]" id="module1"
								style="width: 200; padding: 2 0 0 4;" class="text_c" onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[0]" id="module1_1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" value="1" onkeyup="clearNoNum(this)" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[0]" id="module1_2" value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"  onkeyup="clearNoNum(this)"/>
						</td>
						<td  id="r3_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[0]" id="module1_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r3_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[0]" id="module1_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[1]" id="module2" onkeydown="enter(this);"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[1]" id="module2_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)" />
						</td>
						<td Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[1]" id="module2_2"   value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)" />
						</td>
						<td  id="r4_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[1]" id="module2_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r4_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[1]" id="module2_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>						
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[2]" id="module3"
								style="width: 200; padding: 2 0 0 4;" class="text_c" onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[2]" id="module3_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)"/>
						</td>
						<td Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[2]" id="module3_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>
						<td  id="r5_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[2]" id="module3_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r5_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[2]" id="module3_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>							
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[3]" id="module4"
								style="width: 200; padding: 2 0 0 4;"class="text_c" onkeydown="enter(this);" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[3]" id="module4_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[3]" id="module4_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>	
						<td  id="r5_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[3]" id="module4_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r5_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[3]" id="module4_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>												
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[4]" id="module5"
								style="width: 200; padding: 2 0 0 4;" class="text_c"onkeydown="enter(this);" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[4]" id="module5_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[4]" id="module5_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>	
						<td  id="r6_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[4]" id="module5_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r6_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[4]" id="module5_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>												
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[5]" id="module6"
								style="width: 200; padding: 2 0 0 4;" class="text_c"onkeydown="enter(this);" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[5]" id="module6_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[5]" id="module6_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>
						<td  id="r7_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[5]" id="module6_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r7_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[5]" id="module6_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>							
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[6]" id="module7"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[6]" id="module7_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[6]" id="module7_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>	
						<td  id="r8_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[6]" id="module7_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r8_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[6]" id="module7_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>											
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[7]" id="module8"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[7]" id="module8_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)" />
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[7]" id="module8_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)" />
						</td>
						<td  id="r9_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[7]" id="module8_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r9_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[7]" id="module8_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>						
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[8]" id="module9"
								style="width: 200; padding: 2 0 0 4;" class="text_c" onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[8]" id="module9_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[8]" id="module9_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)" />
						</td>	
						<td  id="r10_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[8]" id="module9_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r10_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[8]" id="module9_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>											
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.moduleData[9]" id="module10"
								style="width: 200; padding: 2 0 0 4;"class="text_c"  onkeydown="enter(this);"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.quotiety[9]" id="module10_1" value="1"
								style="width: 40; padding: 2 0 0 4;" class="text_c" onkeyup="clearNoNum(this)"/>
						</td>
						<td  Class="text_c" align="center" style="border-right:0">
							<input type="text" name="dto.caseCount[9]" id="module10_2"  value="3"
								style="width: 50; padding: 2 0 0 4;" class="text_c"onkeyup="clearNoNum(this)"/>
						</td>
						<td  id="r11_1" Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.scrpCount[9]" id="module10_3"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>
						<td id="r11_2"  Class="text_c" align="center" style="border-right:0;display:none">
							<input type="text" name="dto.sceneCount[9]" id="module10_4"
								style="width: 40; padding: 2 0 0 4;" class="text_c"   onkeyup="clearNoNum(this)" />
						</td>							
					</tr>
					<tr class="ev_mypm">
						<td colspan="6"  align="left" style="border-right:0">
						&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="addchildRd" style="padding: 2 0 0 4;"  id="addchildRd" checked="checked" value="addchild" onclick="javascript:$('command').value='addchild';;$('addBroRd').checked=false;"/><label for="addchild"> <font color="blue">子需求</font></label>
						<input type="radio" name="addBroRd" id="addBroRd"  value="addBro" onclick="javascript:$('command').value='addBro';$('addchildRd').checked=false;"/><label for="addBroRd"><font color="blue">同级需求</font></label>
						</td>
						<td id="r12_1" style="border-right:0;display:none"></td>
						<td id="r12_2"style="border-right:0;display:none"></td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="6" class="text_c" align="center" style="border-right:0">
							<a class="bluebtn" href="javascript:void(0);" id="save_b1"onclick="javascript:cuW_ch.setModal(false);cuW_ch.hide();adjustCreTable('fun');"
					        style="margin-left: 6px;"><span>返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="save_b2"onclick="addNode2db();"
					        style="margin-left: 6px;"><span>确定并继续</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="save_b"onclick="addNode2db('closeFlg');"
					        style="margin-left: 6px;"><span> 确定</span> </a>
						</td>
						<td id="r13_1" style="border-right:0;display:none"></td>
						<td id="r13_2"style="border-right:0;display:none"></td>						
					</tr>
				</table>
			</ww:form>
		 </div>
		</div>
		<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/outlineManager/outLineBase.js"></script>	
	<div id="selPeopleDiv"   class="cycleTask gridbox_light" style="border:0px;display:none;">
	  <div class="objbox" style="overflow:auto;width:100%;">
		  <ww:form theme="simple" method="post" id="assignForm" name="assignForm" namespace="/outLineManager" action="">
		    <ww:hidden id="assignNIds" name="dto.assignNIds"></ww:hidden>
		    <ww:hidden id="userIds" name="dto.userIds"></ww:hidden>
		    <ww:hidden id="parentIdes" name="dto.parentIdes"></ww:hidden>
		     <ww:hidden id="assignType" name="dto.reqType"></ww:hidden>
			<table  class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr>
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr>
				<td  Class="text_c" align="left" style="border-right:0">	
						<font color="blue">分配</font><input type="radio" name="devPer" style="padding: 2 0 0 4;"  id="devPer" checked="checked" value="1" onclick="javascript:$('assignType').value='5';$('devPerL').checked=false;reLoadPeopleData('5')"/><label for="devPer"> <font color="blue">开发人员</font></label>
						<input type="radio" name="devPerL" id="devPerL"  value="3" onclick="javascript:$('assignType').value='4';$('devPer').checked=false;reLoadPeopleData('4')"/><label for="devPerL"><font color="blue">分配人员</font></label>											  
				</td>				
				  <td align="center" style="border-right:0">
				  	   <a class="bluebtn" href="javascript:void(0);" id="selPeople_b"onclick="selPeople();"
					        style="margin-left: 6px;"><span> 确定</span> </a>
				  	  <a class="bluebtn" href="javascript:void(0);" id="fres_btn"onclick="initSleEdPeople(getChecked(),'fresh');"
					        style="margin-left: 6px;"><span> 刷新备选人员</span> </a>
				     
				  </td>
				</tr>
				<tr height="5">
				  <td colspan="2" style="border-right:0">&nbsp;</td>
				</tr>
				<tr height="290">
			    	<td align="center" valign="top" width="160" style="border-right:0">
					  <div id="selGridbox" ></div>
					</td>
			    	<td align="center" valign="top" width="160" style="border-right:0">
					  <div id="seledGridbox"></div>
					</td>
				</tr>
			</table>
		</ww:form>
	  </div>
  </div>
  <ww:include value="/jsp/common/dialog.jsp"></ww:include>	
	</body>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<script type="text/javascript">
		function getkeyCode(e) {
		    var keynum = "";
		    if(_isIE) {
		        keynum = e.keyCode;
		    }else {
		        keynum = e.which;
		    }
		    return keynum;
		}
		function enter(obj){
			obj.onkeydown = function(e) {
				var currKeyCode = "";
		        if(_isIE) 
		        	currKeyCode=getkeyCode(event);
		        else 
					currKeyCode=getkeyCode(e);
				if(currKeyCode==13){
					var fieldId = obj.id;
					var fnum = fieldId.substring(6);
					if(fnum=="10"){
						addNode2db();
					}else{
						$("module" +(parseInt(fnum)+1)).focus();
					}
				}
	    	}
		}
	</script>
</html>

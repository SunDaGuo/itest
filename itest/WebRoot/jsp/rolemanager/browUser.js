	function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
	} 
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
			pageAction(pageNo, pageSize);
		}else if(id == "find"){
			find("findDiv");
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});

	function pageAction(pageNov, pageSizev){
		var gridObj = pmGrid;
		var braObj = pmBar;
		var url = conextPath+"/role/roleAction!roleUserList.action";
		url+="?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.pageSize=" + pageSizev+"&dto.pageNo="+ pageNov;
		if(pageNov>pageCount){
			pmBar.setValue("page", pageNov);
		}
		var ajaxRest = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText;
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		gridObj.clearAll();
	   		gridObj.parse(eval("(" + userJson[1] +")"), "json");
    		setPageNoSizeCount(userJson[0],braObj);
	   		setRowNum(gridObj);
   		}
	}
	function find(divId){
		if(typeof fW_ch=="undefined"){
			findCB();
		}
		fW_ch = initW_ch(fW_ch, divId, false, 460, 150);
		fW_ch.setText("查询");
		$("loginName_f").focus();
	}	
	function findCB(){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		$("groupIds_f").options.length = 1;
			if(groupSel != ""){
				var options = groupSel.split("^");
				for(var i = 0; i < options.length; i++){
					if(options[i].split(";")[0] != "")
						var selvalue = options[i].split(";")[0] ;
						var selable = options[i].split(";")[1];
						$("groupIds_f").options.add(new Option(selable,selvalue));
				}
			}
	}
	function efind(){
		var gridObj = pmGrid;
		var braObj = pmBar;
		var url = conextPath+"/role/roleAction!roleUserList.action";
		url+="?dto.role.roleId="+parent.pmGrid.getSelectedId()+"&dto.pageSize=" + pageSize;
		var ajaxRest = dhtmlxAjax.postSync(url,"findForm").xmlDoc.responseText;
	   	if(ajaxRest=="failed"){
	   		hintMsg("查询时发生错误");
	   		return;
	   	}
	   	var datas = ajaxRest.split("$");
    	gridObj.clearAll();
    	if(datas[1] != ""){
	    	jsons = eval("(" + datas[1] +")");
	    	gridObj.parse(jsons, "json");
	    	setPageNoSizeCount(datas[0],braObj);
	   		setRowNum(gridObj);
    		fW_ch.setModal(false);
    		fW_ch.hide();   	
    	}else{
    		setPageNoSizeCount(datas[0],braObj);
	    	hintMsg("没查到相关记录");
	    }
	}
	function initW_ch(obj, divId, mode, w, h,wId){//初始化 window
		importWinJs();
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w,h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.setDimension(w,h);
			obj.show();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}	
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}

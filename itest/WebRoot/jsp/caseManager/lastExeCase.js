	var pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath +"/dhtmlx/toolbar/images/");
	pmBar.addButton("exeCase",1 , "", "exeCase.gif");
	pmBar.setItemToolTip("exeCase", "执行用例");
	pmBar.addButton("find",2 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	pmBar.addButton("reFreshP",3 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addButton("back",4, "", "back.gif");
	pmBar.setItemToolTip("back", "返回");
	pmBar.addButton("custHome",5, "", "myHome.png");
	pmBar.setItemToolTip("custHome", "设置当前页为我的MYPM主页");
	pmBar.addButton("first", 6, "", "first.gif", "first.gif");
	pmBar.addButton("first", 7, "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious", 8, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider", 9, 80, 1, 30, 1, "", "", "%v");//id, pos, len, valueMin, valueMax, valueNow, textMin, textMax, tip
	pmBar.addButton("next", 10, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last", 11, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page", 12, "", 25);
	pmBar.addText("pageMessage", 13, "");
	pmBar.addText("pageSizeText",14, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'), Array('id4', 'obj', '25'));
	pmBar.addButtonSelect("pageP", 15, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");	
	pmBar.addText("pageSizeTextEnd",16, "条");
	pmBar.addSeparator("sep_swTask3",17);  
	pmBar.addText("sw2TaskTxt",18, "切换项目", "");
	var taskOpts = Array(Array('allTask', 'obj', '所有'), Array('sw2Task', 'obj', '切换到指定项目'));
	pmBar.addButtonSelect("selTaskPage", 19, "所有", taskOpts);
	pmBar.addText("qSearch",20, "编号快查");
	pmBar.addInput("qSearchIpt",21, "", 60);
	pmBar.addSeparator("sep_swTask4",22); 
	pmBar.addButton("currPosition",23, "最近执行的测试用例","hand.gif", "hand.gif"); 
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一隐藏列为是否发布的标识,和模块 id
    pmGrid.setHeader("&nbsp;,编号,用例描述,测试结果,执行人,执行版本,执行日期,成本,项目名称,备注,taskId,moduleId");
    pmGrid.setInitWidths("25,80,*,70,100,80,120,40,120,130,0,0");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,int,str,str,str,int");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,false,false");
    pmGrid.setSkin("light");
    pmGrid.enableAutoHeight(true, 500);
    pmGrid.enableRowsHover(true, "red");
    pmGrid.init();
	initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("OnCheck",doOnCheck);
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    	
    function upGridExeRecord(exeResult){
		var restArr = exeResult.split("$");
		for(var i = 0; i<restArr.length; i++){
			var testRed = restArr[i].split(" ");
			if(pmGrid.getRowIndex(testRed[0])>=0){
				reSetTestRest(testRed);
			}else{
				var RowId = testRed[0].split("_")[0]+"_0";
				testRed[0] = RowId;
				if(pmGrid.getRowIndex(testRed[0])>=0){
					reSetTestRest(testRed);
				}
			}
		}
	}
	
	function reSetTestRest(testRedArr){
		pmGrid.cells(testRedArr[0], 3).setValue(testRest);
		pmGrid.cells(testRedArr[0], 4).setValue(myName);
		pmGrid.cells(testRedArr[0], 5).setValue(testRedArr[1]);
		pmGrid.cells(testRedArr[0], 6).setValue(nowStr);
		pmGrid.cells(testRedArr[0], 9).setValue(testRemark);	
	}
	function getCurCaseExeRecd(){
		var testRedId = pmGrid.getSelectedId();
		var testCaseId = testRedId.split("_")[0];
		var testRedSet = "";
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && items[i].split("_")[0]==testCaseId){
				if (testRedSet == ""){
					testRedSet = items[i];
				}else{
					testRedSet += " " + items[i];
				}
			}
		}
		return testRedSet;
	
	}
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		var rest = pmGrid.cells(rowId, 3).getValue();
		try{
			if(rest=='待审核'||rest=='不适用'||rest=='待修正'){
				pmBar.disableItem("exeCase");
			}else{
				pmBar.enableItem("exeCase");
			}		
		}catch(err){}
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		pmGrid.setSelectedRow(rowId);
	}
	function colTypeReset(){
		pmGrid.setColTypes("ra,link,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ra,link,link,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
		
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:relaBug()' title=\"关联BUG\">"+getTttle2(i,1)+"</a>";
			if(getTttle2(i,2).indexOf("<a href")<0){
				pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:viewDetal()' title='查看明细----"+getTttle2(i,2)+"'>"+getTttle2(i,2)+"</a>";
			}
		}
		sw2Link();
	}	
	
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	function quickQuery(){
		var url =  conextPath+"/caseManager/caseManagerAction!quickQueryLastExeCase.action?dto.testCaseInfo.testCaseId="+pmBar.getValue("qSearchIpt");
		var ajaxRest = postSub(url,"");
		if(ajaxRest==""){
			hintMsg("没查到相关记录");
			return;
		}
		pmGrid.clearAll();
		colTypeReset();
		var jsons = eval("(" + ajaxRest +")");
		pmGrid.parse(jsons, "json");
		cusSetPageNoSizeCount();
	   	loadLink();
	}
    function cusSetPageNoSizeCount(){
    	var tBar = pmBar;
		tBar.setItemText("pageP", pageSize);
		tBar.setValue("page", 1);
		tBar.setMaxValue("slider", 1, "");
		tBar.setItemText("pageMessage", "/ " + 1);
		tBar.setValue("slider", 1);
		tBar.disableItem("first");
		tBar.disableItem("pervious");
		tBar.disableItem("next");
		tBar.disableItem("last");
		tBar.disableItem("slider");		
	}
	pmBar.attachEvent("onEnter", function(id, value) {
		if(id=="qSearchIpt"){
			if(!isDigit(pmBar.getValue("qSearchIpt"), false)&&pmBar.getValue("qSearchIpt")!=""){
				pmBar.setValue("qSearchIpt", "");
				return;
			}
			quickQuery();
			return ;
		}
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id=="sw2Task"){
			openSwTaskList();
		}else if(id=="allTask"){
			$("taskId").value = "";
			sw2AllTask();
		}if(id=="custHome"){
			dhtmlxAjax.post(conextPath+"/commonAction!setLastExeCaseAsHome.action");
			parent.setCheckboxUnchk('lastExeCaseHome');
		}
	});
	
	function go2SpeTaskCaseExeRed(){
		var url =conextPath+"/caseManager/caseManagerAction!lastExeCase.action?dto.taskId="+$("taskIdF").value;
		url += "&dto.pageNo=1" +"&dto.pageSize=10";
		var rest = postSub(url,"");
		if(rest=="failed"){
			hintMsg("加载数据发生错误");
			return;		
		}
		pmBar.setItemText("selTaskPage", "切换到指定项目");
		pmGrid.clearAll();
		$("listStr").value = rest;
		colTypeReset();
		initGrid(pmGrid,"listStr");
		loadLink();
	}
	function sw2AllTask(){
		var url =conextPath+"/caseManager/caseManagerAction!lastExeCase.action";
		url += "?dto.pageNo=1" +"&dto.pageSize=10";
		var rest = postSub(url,"");
		if(rest=="failed"){
			hintMsg("加载数据发生错误");
			return;		
		}
		pmBar.setItemText("selTaskPage", "所有");
		pmGrid.clearAll();
		$("listStr").value = rest;
		colTypeReset();
		initGrid(pmGrid,"listStr");
		loadLink();
	}
	var testProSelWin;
	function openSwTaskList(){
		if(typeof(testProSelWin)!="undefined" &&testProSelWin.isHidden()){
	    	testProSelWin.show();
	    	testProSelWin.bringToTop();
	    	testProSelWin.setModal(true);
	    	return;			
		}
		testProSelWin = initW_ch(testProSelWin, "", true, 930, 500,'testProSelWin');
		var url = conextPath+"/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=lastExeCaseList";
		url += "&dto.taskIdField=taskIdF&dto.taskNameField=taskName";
		testProSelWin.attachURL(url);
		testProSelWin.setText("请选择测试项目");	
    	testProSelWin.show();
    	testProSelWin.bringToTop();
    	testProSelWin.setModal(true);			
	}
	pmBar.attachEvent("onClick", function(id) {
		if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3"|| id == "id4"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}else if(id=="back"){
			parent.cuW_ch.setModal(false);
			parent.cuW_ch.hide();
		}else if(id =="reFreshP"){
  			pageAction(pageNo, pageSize);
		}else if(id == "find"){ 
			findInit();
		}else if(id=="exeCase"){
			exeInit();
		}
	});

	var relBugW_ch;
	function relaBug(){
		var testCaseId = pmGrid.getSelectedId().split("_")[0];
		var rowNum = pmGrid.getRowIndex(pmGrid.getSelectedId());
		var url=conextPath+"/bugManager/relaCaseAction!loadRelaBug.action?dto.taskId="+pmGrid.cells2(rowNum,10).getValue();
		url = url +"&dto.moduleId="+pmGrid.cells2(rowNum,11).getValue()+"&dto.testCaseId="+testCaseId+"&dto.isExeRela=1"
		relBugW_ch = initW_ch(relBugW_ch, "", true, 890, 520,'relBugW_ch');
		relBugW_ch.attachURL(url);
		relBugW_ch.setText("关联BUG---点击序号查看明细---可跨页选择后关联");	
	    relBugW_ch.show();
	    relBugW_ch.bringToTop();
	    relBugW_ch.setModal(true);	
	}
		
	function exeInit(){
		var adtCaseId = pmGrid.getSelectedId();
		if(adtCaseId==null){
			hintMsg("请选择要执行的用例");
			return;
		}
		adtCaseId = adtCaseId.split("_")[0];
		url = conextPath+"/caseManager/caseManagerAction!upInit.action?dto.testCaseInfo.testCaseId="+adtCaseId;	
		var rowNum = pmGrid.getRowIndex(pmGrid.getSelectedId());
		var taskId = pmGrid.cells2(rowNum,10).getValue();
		url +="&dto.taskId=" +taskId;
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("初始化失败");
			return;
		}	
		setUpInfo(ajaxResut);	
		dW_ch = initW_ch(dW_ch,  "createDiv", true,700, 430);
		dW_ch.setDimension(700, 440);
		$("pass_b").style.display="";
		$("failed_b").style.display="";
		$("block_b").style.display="";
		$("block2_b").style.display="";
		$("saveBtn").style.display="";
		$("verTr").style.display="";
		$("remark").value="";
		dW_ch.setText("执行用例");
		$("cUMTxt").innerHTML =  "&nbsp;";
		adjustTable("createTable");
		loadFCK();
		loadVers();
	}
	var verSelData = "";
	function loadVers(){
		if(verSelData!="")
			return;
		var verUrl = conextPath+"/testTaskManager/testTaskManagerAction!loadVerSel.action";
		verSelData  = dhtmlxAjax.postSync(verUrl, "").xmlDoc.responseText;
		$("sel_vers").options.length = 1;
		if(verSelData != ""){
			var options = verSelData.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					var selable = options[i].split(";")[1];
					$("sel_vers").options.add(new Option(selable,selvalue));
			}
		}
	}
	
	function exeCase(rest){
		if($("sel_vers").value=="-1"){
			hintMsg("请选择执行版本");
			return ;
		}else if(isWhitespace($("remark").value)&&(rest=='4'||rest=='5')){
			hintMsg("请填写备注");
			return ;		
		}
		var url=conextPath+"/caseManager/caseManagerAction!exeCase.action?dto.testCaseInfo.testCaseId="+$("testCaseId").value;
		url+="&dto.testCaseInfo.moduleId="+$("moduleId").value+"&dto.testCaseInfo.testStatus="+rest+"&dto.exeVerId="+$("sel_vers").value;
		url+="&dto.remark="+$("remark").value;
		ajaxResut = postSub(url,"");
		//因为ID不唯一，所以拼了了版本ID，不能直接用testCaseId
		var rowId = pmGrid.getSelectedId();
	    if(ajaxResut=="success"){	
	    	pmGrid.cells(rowId,3).setValue(result2Str(rest));
	    	pmGrid.cells(rowId,4).setValue(myName);
	    	pmGrid.cells(rowId,5).setValue($("sel_vers").options[$("sel_vers").selectedIndex].text);
	    	pmGrid.cells(rowId,6).setValue(nowStr);
	    	pmGrid.cells(rowId,7).setValue($("weight").value);
	    	pmGrid.cells(rowId,9).setValue($("remark").value);
			dW_ch.hide();
			dW_ch.setModal(false);
			$("sel_vers").value="-1";
	    }else{
	    	hintMsg("保存数据发生错误");
	    }		
	}	
	function result2Str(rest){
		if(rest=="2"){
			return "通过";
		}else if(rest=="3"){
			return "未通过";
		}else if(rest=="4"){
			return "不适用";
		}else if(rest=="5"){
			return "阻塞";
		}
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+'/caseManager/caseManagerAction!lastExeCase.action';
		url += "?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		$("listStr").value = postSub(url,"findForm");
		colTypeReset();
		initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
		loadLink();
	}	
	function viewDetal(){
		url = conextPath+"/caseManager/caseManagerAction!viewDetal.action?dto.testCaseInfo.testCaseId="+pmGrid.getSelectedId().split("_")[0];	
		var rowNum = pmGrid.getRowIndex(pmGrid.getSelectedId());
		var taskId = pmGrid.cells2(rowNum,10).getValue();
		url +="&dto.taskId=" +taskId;		
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg("初始化失败");
		}
		$("pass_b").style.display="none";
		$("failed_b").style.display="none";
		$("block_b").style.display="none";
		$("block2_b").style.display="none";
		$("saveBtn").style.display="none";
		$("verTr").style.display="none";
		dW_ch = initW_ch(dW_ch,  "createDiv", true,700, 430);
		dW_ch.setText("用例明细");
		setUpInfo(ajaxResut);	
		adjustTable("createTable");
	}
	var oEditor, operData="";
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData(operData) ;
			return;
		}
		importFckJs();
    	var pmEditor = new FCKeditor('operDataRichText') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('operDataRichText') ;
		oEditor.SetData(operData) ;
	}
	function setUpInfo(updInfo){
		if(updInfo=="")
			return;
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				if(currInfo[0]=="typeSelStr"||currInfo[0]=="priSelStr"){
					var selStr = currInfo[1];
					var selId="priId";
					if(currInfo[0]=="typeSelStr"){
						selId="caseTypeId";
					}
					loadSel(selId,selStr);	
				}else{
					valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
					eval(valueStr);
					if(currInfo[0]=="operDataRichText"){
						operData=currInfo[1];
					}	
					if(currInfo[0]=="expResult"){
						$("expResultOld").value=currInfo[1];
					}
					if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
						$("currAttach").style.display="";
						$("currAttach").title="附件";
						//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
					}	
					if(currInfo[0]=="attachUrl"&&currInfo[1]==""){
						$("currAttach").style.display="none";
					}	
				}
			}
		}
		loadFCK();
	}
	function loadSel(selId,selStr,splStr){
		if(selStr==""){
			return;
		}
		$(selId).options.length = 1;
		var options ;
		if(splStr){
			options = selStr.split(splStr);
		}else{
			options = selStr.split("$");
		}
		for(var i = 0; i < options.length; i++){
			var optionArr = options[i].split(";");
			if(optionArr[0] != "")
				var selvalue = optionArr[0] ;
				var selable = optionArr[1];
				$(selId).options.add(new Option(selable,selvalue));
		}	
	}
	var DropList ="";
	function findInit(){
		if(DropList!=""){
			fW_ch.show();
			fW_ch.bringToTop();
			return;
		}
		var url = conextPath+"/caseManager/caseManagerAction!dropListWithVer.action?dto.taskId="+$("taskId").value;
		var ajaxResut = postSub(url,"");
		if(ajaxResut.indexOf("success")==0){
			DropList = ajaxResut;
			var restArr = ajaxResut.split("$");
			loadSel("caseTypeIdF",restArr[1],"^");
			loadSel("priIdF",restArr[2],"^");
			//loadSel("exeVerF",restArr[3],"^");
		}
		fW_ch = initW_ch(fW_ch, "findDiv", true, 555, 200);
		fW_ch.button("close").attachEvent("onClick", function(){
			if(typeof(mypmCalendar_ch)!='undefined')
				mypmCalendar_ch.hide();
			fW_ch.setModal(false);
			fW_ch.hide();
		});
		fW_ch.setText("查询");	
	}
	function findExe(){
		var url = conextPath+'/caseManager/caseManagerAction!lastExeCase.action';
		var ajaxRest = postSub(url, "findForm");
		if(ajaxRest=="failed"){
			hintMsg("查询失败");	
		}else{
			if(ajaxRest.split("$")[1]!=""){
				$("listStr").value=ajaxRest;
				colTypeReset();
				initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
				loadLink();
				if(typeof(mypmCalendar_ch)!='undefined')
					mypmCalendar_ch.hide()
				fW_ch.hide();
				fW_ch.setModal(false);
				//cp2Pform();
				return;		
			}
			hintMsg("没有查询到相关记录");
		}	
	}

	function save(){
		if(subChk()){
			$("testData").value=html2PlainText("operDataRichText");
			var testDataV = $("testData").value;
			var mdPath = $("mdPath").value;
			if(mdPath.replace(/\s+$|^\s+/g,"")==testDataV.replace(/\s+$|^\s+/g,"")){
				hintMsg("请填写过程及数据");
				return;
			}
			var url = conextPath+"/caseManager/caseManagerAction!upCase.action";
			var ajaxResut = postSub(url,"createForm");
			if(ajaxResut.indexOf("^") != -1){
				ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
				var result = ajaxResut.split("^");
				if(result[0] == "success"){
					colTypeReset();
					var rowId = pmGrid.getSelectedId();
					var rowData = "0,,"+$("testCaseDes").value+",";
					rowData +=pmGrid.cells(rowId,3).getValue()+",";
					rowData +=pmGrid.cells(rowId,4).getValue()+",";
					rowData +=pmGrid.cells(rowId,5).getValue()+",";
					rowData +=pmGrid.cells(rowId,6).getValue()+",";
					rowData +=$("weight").value+",";
					rowData +=pmGrid.cells(rowId,8).getValue()+",";
					rowData +=pmGrid.cells(rowId,9).getValue();
					pmGrid.deleteRow(rowId);
					pmGrid._addRow(rowId,rowData,0);	
					//setRowNum(pmGrid);
					loadLink();
					pmGrid.setSelectedRow(result[1]);	
					pmGrid.setSizes();	
					$("cUMTxt").innerHTML =  "修改用例成功";
					return;
				}else if(result[0]=="failed"&&ajaxResut.indexOf("^")>0){
					hintMsg(result[1]);
					return;	
				}else{
					$("cUMTxt").innerHTML =  "修改用例失败";
					return;	
				}
			}
			$("cUMTxt").innerHTML =  "修改用例失败";
			return;	
		}
	}
	function subChk(){
		if($("caseTypeId").value=="-1"){
			hintMsg("请选择用例类型");
			return false;
		}else if($("priId").value=="-1"){
			hintMsg("请选择用例优先级");
			return false;
		}else if(isAllNull($("testCaseDes").value)){
			hintMsg("请填写用例描述");
			return false;
		}else if(checkIsOverLong($("testCaseDes").value,60)==true){
			hintMsg("用例描述不能超过60个字符");
			return false;
		}else if(!isAllNull($("expResult").value)&&checkIsOverLong($("expResult").value,400)){
			hintMsg("预期结果不能超过400个字符");
			return false;
		}else if(oEditor.GetXHTML().trim()=="<br>"||oEditor.GetXHTML().trim()==""){
			hintMsg("请填写过程及数据");
			oEditor.Focus();
			return false;		
		}else if(oEditor.GetXHTML().trim()!=""){
			$("operDataRichText").value=oEditor.GetXHTML().trim();
			var endStr = "";
			if($("operDataRichText").value.endWith("</strong>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</strong>");
				endStr = "</strong>";
			}
			if($("operDataRichText").value.endWith("</b>")){
				$("operDataRichText").value=$("operDataRichText").value.trim("</b>");
				endStr = "</b>";
			}
			$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");
			$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");
			for(var i=1; i<31;i++){
				$("operDataRichText").value = $("operDataRichText").value.trim("<br>");
				$("operDataRichText").value = $("operDataRichText").value.trim("&nbsp;");	
				$("operDataRichText").value = $("operDataRichText").value.replace(/\s+$|^\s+/g,"");		
			}
			$("operDataRichText").value +=endStr;
			if(checkIsOverLong($("operDataRichText").value,1200)){
				hintMsg("过程及数据不能超过800个字符");
				return false;				
			}	
		}
		return true;
	}
	if(!_isIE){
		dW_ch = initW_ch(dW_ch,  "createDiv", true,2, 2,"dW_ch");
		dW_ch.hide();
		dW_ch.setPosition(1, 1);
		dW_ch.setModal(false);
       }

	
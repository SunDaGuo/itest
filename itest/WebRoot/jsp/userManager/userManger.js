	var currStatus;
	pmBar.attachEvent("onClick", function(id){
		if(id == "find"){
		    findM(pageBreakUrl,"findDiv","",460, 155,findCB());
		}else if(id == "new"){
			if(_isIE){
				addUpInit(id, "createDiv",470, 280,"","","",addUpInitCB("new"));
			}else{
				addUpInit(id, "createDiv",470, 310,"","","",addUpInitCB("new"));
			}
		}else if(id == 'update'){
			var adHigh = 249;
			if(pmGrid.getSelectedId()==myId){
				if(_isIE){
					adHigh = adHigh+75;
				}else if(_isFF){
					adHigh = adHigh+100;
				}
			}else{
				if(navigator.userAgent.indexOf("Chrome")>0){
					adHigh = adHigh+10;
				}
			}
			
			//这里的url 取标签中urlVarName指定的变量名
			addUpInit(id, "createDiv",470, adHigh,"cUMTxt",upUrl,pmGrid,addUpInitCB("update"));
			//ps.update($("password").value);
		}else if(id == 'delete'){
			if(getChecked(pmGrid)==""){
				hintMsg("请选择要逻辑删除的记录");
				return;
			}
			cfDialog("ldeleteExe","您确定逻辑删除选择的记录?",false);
		}else if(id == 'deleteR'){
			if(getChecked(pmGrid)==""){
				hintMsg("请选择要删除的用户");
				return;
			}
			if(pmGrid.cells(getChecked(pmGrid),10).getValue()==2){
				hintMsg("管理人员不可删除");
				return;
			}
			cfDialog("deleteExe","您确定删除选择的用户?",false);
		}else if(id=="chgSta"){
			if(getChecked(pmGrid)==""){
				hintMsg("请选择要切换状态的用户");
				return;
			}
			currStatus = pmGrid.cells(pmGrid.getSelectedId(),9).getValue();
			if(currStatus=="禁用"){
				cfDialog("swUserStatus","您确定启用选择的用户?",false);	
				currStatus = 1;
			}else{
				cfDialog("swUserStatus","您确定禁用选择的用户?",false);
				currStatus = 0;
			}
					
		}else if(id == 'importUser'){
			importW_ch = initW_ch(importW_ch,  "importUserDiv", false, 400, 125, "", "导入用户");
			importW_ch.setText("导入用户");
			$("importUserText").innerHTML = "";
		}else if(id == 'reSetPwd'){
			if(getChecked(pmGrid)==""){
				hintMsg("请选择要重置密码的用户");
				return;
			}		
			cfDialog("reSetPwdExe","您确定要重置所选用户密码为mypm?",false);
		}else if(id == 'setMgrPersion'){
			if(getChecked(pmGrid)==""){
				hintMsg("请先选择一用户");
				return;
			}
			if(pmGrid.cells(getChecked(pmGrid),10).getValue()==2){
				cfDialog("chgPersonMgr","您确定要撤销所选用户的管理人员设置?",false);
			}else{
				cfDialog("chgPersonMgr","您确定要设置所选用户为管理人员?",false);
			}
		}
	});	
	
	function chgPersonMgr(){
		var userId = pmGrid.getSelectedId();
		var currMgrFlg = pmGrid.cells(userId,10).getValue();
		var swUrl = conextPath+"/userManager/userManagerAction!chgPersonMgr.action?dto.userIds="+userId+"&dto.user.isAdmin="+currMgrFlg;
		var result = dhtmlxAjax.postSync(swUrl, "").xmlDoc.responseText;
		if(result=="success"){
			if(currMgrFlg=="2"){
				pmGrid.cells(userId,4).setTextColor("black");
				pmGrid.cells(userId,10).setValue("0");
				pmBar.setItemToolTip("setMgrPersion", "设置用户为管理人员使其可查看任何项目");
			}else{
				pmGrid.cells(userId,4).setTextColor("blue");
				pmGrid.cells(userId,10).setValue("2");
				pmBar.setItemToolTip("setMgrPersion", "撤销用户管理人员设置使其只可查看本人参与的项目");
			}	
			clsoseCfWin();		
		}else if("deny"==result){
			hintMsg("您不被允许当前操作");
		}else{
			hintMsg("操作失败");
		}		
	}
	function reSetPwdExe(){
		var userId = pmGrid.getSelectedId();
		var swUrl = conextPath+"/userManager/userManagerAction!update2Init.action?dto.userIds="+userId;
		var result = dhtmlxAjax.postSync(swUrl, "").xmlDoc.responseText;
		if(result=="success"){
			hintMsg("密码己重置为mypm");
		}else{
			hintMsg("密码重置失败");
		}
	}
	function swUserStatus(){
		var userId = pmGrid.getSelectedId();
		var swUrl = conextPath+"/userManager/userManagerAction!swUserStatus.action?dto.user.id="+userId;
		swUrl = swUrl + "&dto.user.status="+currStatus;
		var result = dhtmlxAjax.postSync(swUrl, "").xmlDoc.responseText;
		if(result=="success"){
			if(currStatus==0)
				pmGrid.cells(userId,9).setValue("禁用");
			else
			  pmGrid.cells(userId,9).setValue("启用"); 
			clsoseCfWin();
			return;
		}else
			hintMsg("切换用户状态发生错误");
		return;
	}
	function getChecked(GridObj){
		selectItems = "";
		var currGrid = pmGrid;
		if(typeof(GridObj)!="undefined"){
			currGrid = GridObj;
		}
		var allItems = currGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && currGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}
	function deleteExe(){
		var delUrl = conextPath+"/userManager/userManagerAction!ldeleteUser.action?dto.user.id=";
		deleteM(delUrl,pmGrid);
	}
	function ldeleteExe(){
		deleteM(ldelUrl,pmGrid);
	}
	function addUpInitCB(id){
		$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
		$("chgPwdFlg").value="0";
		if(id=="new"){
			ps.copyToStyle(0);
			$("inVoldPwdR").style.display = "none";
			$("userId").value = "";
			$("groupNames").value = "";
			$("groupIds").value = "";
			$("cuU_b").value = " 确定 ";
			$("questionR").style.display = "";
			$("answerR").style.display = "";	
			$("cuU2_b").style.display = "";
			$("loginName").disabled=false;
		}else if(id=="update"){
			$("groupNames").value = "";
			$("groupIds").value = "";
			$("questionR").style.display = "none";
			$("answerR").style.display = "none";
			$("cuU2_b").style.display = "none";
			if(pmGrid.getSelectedId()==myId){
				$("questionR").style.display = "";
				$("answerR").style.display = "";
			}
			$("inVoldPwdR").style.display = "";
			$("cuU_b").value = " 修改 ";
			$("loginName").disabled=true;
		}	
		adjustTable("createTab");
		return true;
	}
	function popMutlGridCB(){
		suW_ch = initW_ch(suW_ch, null, true, 220,200);
		if(typeof popGrid == "undefined"){
			popGrid = suW_ch.attachGrid();
			suW_ch.setText("选择组");
			popGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			popGrid.setHeader("选择,序号,组名");
			popGrid.setInitWidths("50,50,115");
	    	popGrid.setColAlign("center,left,left");
	    	popGrid.setColTypes("ch,ro,ro");
	   		popGrid.setColSorting("Str,int,str");
	   		popGrid.enableTooltips("false,false,true");
	        popGrid.init();
	    	popGrid.setSkin("light");		
		}
    	return popGrid;
	}
	var repChkCount=0,reName="0",reChk=0;
	function loginNameChk(){
		if(isWhitespace($("loginName").value)){
			$("cUMTxt").innerHTML="&nbsp;";
			return;
		}
		if(repChkCount==5)
			return;
		$("cuU2_b").style.display="none";
		$("cuU_b").style.display="none";
		var url = conextPath+"/commonAction!regisChk.action?dto.objName=User&dto.nameVal="+$("loginName").value;
		url+="&dto.namePropName=loginName&dto.idPropName=id&&dto.idPropVal="+$("userId").value;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		repChkCount++;
		if(ajaxRest=="true"){
			$("cUMTxt").innerHTML = "所填登录帐号重复";
			reName="1";
			$("cuU2_b").style.display="";
			$("cuU_b").style.display="";
			return;
		}
		$("cUMTxt").innerHTML="&nbsp;";
		$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
		reName="0";
	}
	var mailChkCount=0,reMail="0";
	function mailChk(){
		if(isWhitespace($("email").value)){
			$("cUMTxt").innerHTML="";
			return;
		}
		if(mailChkCount==5){
			return;
		}
		$("cuU2_b").style.display="none";
		$("cuU_b").style.display="none";
		var url = conextPath+"/commonAction!reNameChk.action?dto.objName=User&dto.nameVal="+$("email").value;
		url+="&dto.namePropName=email&dto.idPropName=id&&dto.idPropVal="+$("userId").value;
		var ajaxRest = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		mailChkCount++;
		if(ajaxRest=="true"){
			$("cUMTxt").innerHTML = "所填电子信箱己被使用";
			reMail="1";
			return;
		}		
		reMail="0";
		$("cUMTxt").innerHTML="";
		if($("userId").value=="")
			$("cuU2_b").style.display="";
		$("cuU_b").style.display="";
	}
	function addUpCB(){
		if(isWhitespace($("loginName").value)){
			$("cUMTxt").innerHTML = "请填写登录帐号";
			$("loginName").focus();
			return false;
		}else if(isWhitespace($("password").value)){
			$("password").focus();
			$("cUMTxt").innerHTML = "请输入登录密码";
			return false;
		}else if(!checkIsOverLong($("password").value,5)){
			$("password").focus();
			$("cUMTxt").innerHTML = "密码不能少于6位";
			return false;
		}else if($("password").value != $("vpassword").value){
			$("vpassword").focus();
			$("cUMTxt").innerHTML = "确认密码不正确";
			return false;
		}else if($("userId").value != ""&&isWhitespace($("voldPwd").value)){
			$("voldPwd").focus();
			$("cUMTxt").innerHTML = "请输入原密码";
			return false;			
		}else if(isWhitespace($("name").value)){
			$("cUMTxt").innerHTML = "请输入真实姓名";
			$("name").focus();
			return false;
		}else if(isWhitespace($("email").value)){
			$("cUMTxt").innerHTML = "请输入邮箱";
			$("email").focus();
			return false;
		}else if(!isEmailAddress($("email").value)){
			$("cUMTxt").innerHTML = "管理员邮箱格式不正确";
			$("email").focus();
			return false;		
		}else if(checkIsOverLong($("employeeId").value,20)){
			$("cUMTxt").innerHTML = "员工编号不能超过20位";
			$("employeeId").focus();
			return false;		
		}else if(checkIsOverLong($("officeTel").value,20)){
			$("cUMTxt").innerHTML = "办公电话不能超过20位";
			$("officeTel").focus();
			return false;			
		}else if(checkIsOverLong($("tel").value,20)){
			$("cUMTxt").innerHTML = "联系电话不能超过20位";
			$("tel").focus();
			return false;			
		}else if(checkIsOverLong($("email").value,32)){
			$("cUMTxt").innerHTML = "电子信箱不能超过32位";
			$("email").focus();
			return false;				
		}else if(checkIsOverLong($("question").value,20)){
			$("cUMTxt").innerHTML = "密码问题不能超过20位";
			$("question").focus();
			return false;			
		}else if(checkIsOverLong($("answer").value,50)){
			$("cUMTxt").innerHTML = "密码答案不能超过50位";
			$("answer").focus();
			return false;			
		}else if(reMail=="1"){
			$("cUMTxt").innerHTML = "所填电子信箱己被使用";
			return false;
		}else if(reName=="1"){
			$("cUMTxt").innerHTML = "所填管理员登录帐号己被注册";
			return  false;
		}else if(!speCharChk("createForm")){
			$("cUMTxt").innerHTML = "不能含特殊字符";
			return  false;		
		}
		return true;
	}
	function findCB(){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		var selInitVal = $("groupIds_f").value;
		var haveInitVal = false;
		$("groupIds_f").options.length = 1;
		if(groupSel != ""){
			var options = groupSel.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					if(selvalue==selInitVal){
						haveInitVal = true;
					}
					var selable = options[i].split(";")[1];
					$("groupIds_f").options.add(new Option(selable,selvalue));
			}
		}
		if(haveInitVal)
			$("groupIds_f").value=selInitVal;
	}
	function chgPwd(){
		var oldPwd=$('oldPwd').value;
		if(oldPwd!=$("password").value){
			$('voldPwd').value='';
			$("chgPwdFlg").value=$("userId").value;
		}else{
			$('voldPwd').value=oldPwd;
		}
	} 
	function popMutlGrid(url,passValId,passNameId,callBack){
		if(""==url){
			hintMsg("您没有操作权限");
			return;
		}
		if(suW_ch.button("dock") == null){
			suW_ch.addUserButton("dock", 0, "选择", "dock");
			suW_ch.button("dock").attachEvent("onClick", function(){
			var Ids = "";
			var Names = "";
			var allItems = popGrid.getAllItemIds();
			var items = allItems.split(',');
			for(var i = 0; i < items.length; i++){
				if(items[i] != "" && popGrid.cells(items[i], 0).getValue() == 1){
					if (Ids == ""){
						Ids = items[i];
						Names =  popGrid.cells(items[i], 2).getValue();
					}else{
						Ids += "," + items[i];
						Names += "," + popGrid.cells(items[i], 2).getValue()
					}
				}
			}
			$(passNameId).value = Names;
			$(passValId).value = Ids ;
			suW_ch.hide();
			suW_ch.setModal(false);
			});
		}
		popGrid.attachEvent("onCheckbox",function(rowId,cellInd,state){
			popGrid.cells(rowId,0).setValue(true);
			return true;
		});
    	var ajaxResut = dhtmlxAjax.postSync(url, "");
    	popGrid.clearAll();
   		var jsonText = ajaxResut.xmlDoc.responseText;
    	if(jsonText != ""){
    		jsonText = jsonText.replace(/[\r\n]/g, "");
   			popGrid.parse(eval("(" + jsonText +")"), "json");
   		}
   		suW_ch.bringToTop();
	}
	function isEmailAddress(value, allowEmpty){
	  var strText = new String(value);
	  if (allowEmpty == true && value.length == 0) {
	    return true;
	  }
	  var strPattern = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  var strPatt = /[^a-zA-Z0-9-@\._]/;
	  if (strText.match(strPatt)) {
	     return false;
	  }
	  aryResult = strText.match(strPattern);
	  if(aryResult == null) {
	  return false;
	  } else {
	  return true;
	 }
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url = conextPath+"/userManager/userManagerAction!userList.action?dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var userJson = dhtmlxAjax.postSync(url, "findForm").xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum(pmGrid);
		    var currrCount = pmGrid.getRowsNum();
			for(var i = 0; i < currrCount; i++){
				if(pmGrid.cells2(i,10).getValue()=="2"&&pmGrid.cells2(i,4).getValue()!="admin")
					pmGrid.cells2(i,4).setTextColor("blue");
			}
   		}

	}
  function speCharChk(formId){
	var form = document.getElementById(formId);
    var elements = form.elements;  
    for (i = 0; i < elements.length; ++i) {
      var element = elements[i];
      if(element.type == "text" || element.type == "textarea" || element.type == "hidden"){
      	if(element.id=="groupNames"||element.id=="groupIds"||element.id=="voldPwd"||element.id=="password"||element.id=="vpassword"||element.id=="insertDate")
      		continue;
      	if(includeSpeChar(element.value)){
      		return false;
      	}
      }
     }
    return true;
  }
  function includeSpeChar(StrVal){
  	var speStr="~ ` ! # $ % ^ & * ( ) [ ] { } ; ' : \" , ， < >";
  	var speStrArr = speStr.split(" ");
  	if(typeof(StrVal)=="undefined"||isWhitespace(StrVal))
  		return false;
  	for(var i=0; i<speStrArr.length; i++){
  		if(StrVal.indexOf(speStrArr[i])>=0){
  			return true;
  		}
  	}
  	return false;
  }
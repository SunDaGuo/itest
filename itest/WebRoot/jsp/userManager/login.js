		var tryCount=0, doubleEntry = true;
		var loginPage = true;
		function loginCheck(autoFlg){
			if(isWhitespace($("loginlName").value)){
	   			$("loginMessage").innerHTML = "请填写用户名";
	   			$("loginlName").focus();
	   			return;
	  		}
	  		if(isWhitespace($("loginPwd").value)){
	  			$("loginMessage").innerHTML = "请填写密码";
	  			$("loginPwd").focus();
	  			return;
	  		}
	  		if (validCodeFlg=="true"&&isWhitespace($("viewCode").value)){
				$("loginMessage").innerHTML = "请填写验证码";
				$("viewCode").focus();
				return;
			}
			if(tryCount>=5){
				$("loginMessage").innerHTML = "5次登录错误，当前页不许再登录";
				return;
			}
			if(!doubleEntry)return;doubleEntry = false;
			var url = conextPath + "/userManager/userManagerAction!login.action";
			if(typeof(autoFlg)!="undefined"){
				url = conextPath + "/userManager/userManagerAction!autoLogin.action";
			}
			var result = dhtmlxAjax.postSync(url, "loginForm").xmlDoc.responseText;
			if(result.indexOf("loginMYPMsuccess")==0){
				if($("isCookieLoginId").checked){
					setCookie("loginId=" + escape($("loginlName").value), 2592000000);
					setCookie("attachInfo=" + escape($("loginPwd").value), 2592000000);
					setCookie("autoLogin=autoLogin" , 2592000000);
				}else{
					setCookie("loginId=", 2592000000);
					setCookie("attachInfo=", 2592000000);
					setCookie("autoLogin=" , 2592000000);
				}			
				$("loginlName").value = "";
			    $("loginPwd").value = "";
			    $("viewCode").value = "";
				var myHome=result.split("^")[1];
				goToMypm(myHome,autoFlg);
	   	 		$("loginMessage").innerHTML = "&nbsp;";
			    $("isCheckViewCode").value = "";
			    $("validCodeImage").style.display="none";
			    $("loginButton").style.display="none";
			    $("resetButton").style.display="none";
			    $("loginMessage").innerHTML = "&nbsp;";
			     closeLoginWin();
			}else if(result=="viewCdError"){
				doubleEntry = true;
				$("loginMessage").innerHTML = "输入验证码不匹配";
			}else if(result=="overdue"){
				doubleEntry = true;
				$("loginMessage").innerHTML = "会话过期请重填验证码";
				$("validCodeImage").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
			}else if(result.indexOf("1")==0){
				$("loginMessage").innerHTML = "在线用户数己达免费版50上限";
				return;
			}else{
				doubleEntry = true;
				tryCount++;
				if(tryCount>=5){
					if(_isIE){
						window.opener = null;
						window.open('','_parent','');
						window.close();
					}
					$("loginMessage").innerHTML = "5次登录错误，当前页不许再登录";
					$("validCodeImage").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
					$("validCodeImage").style.display="none";
				}else
					$("loginMessage").innerHTML = "用户名或密码错误,还有"+(5-tryCount)+"次登录机会";
				if(validCodeFlg=="true")$("validCodeImage").src=conextPath+"/img/validcodeimage.jsp?test=" +(new Date()).getTime();
			}
		}
		function reSetLogin(chkObj){
			if(!chkObj.checked){
				setCookie("loginId=", 2592000000);
				setCookie("attachInfo=", 2592000000);			
			}
		}
		var clsWin = true;
		function closeLoginWin(){
			if(navigator.userAgent.indexOf("Chrome")>0)
				return;
			if (navigator.appVersion.indexOf("MSIE 8.0") <0){
				window.opener = null; 
				window.open('','_parent','');
				window.close();	
			}
		}
		
		function goToMypm(myHome,autoFlg){
			if(document.referrer.indexOf("navigate?reLogin=1")>=0){
				if(myHome.length>20||myHome==""||myHome=="null"){
					//window.location.href=conextPath +"/jsp/common/main.jsp";;
				}else{
					window.location.href=conextPath +"/jsp/myhome/myHome.jsp"; 
				}				
			}
			if(myHome.length>20||myHome==""||myHome=="null"){
				if(_isIE){
					window.open(conextPath + "/jsp/common/main.jsp", "_blank", "width=" + window.screen.width + ",height=" + (window.screen.height - 90) + ",menubar=no,toolbar=no,location=no,top=0,left=0,directories=no,status=no,scrollbars=yes,resizable=no");
				}else
					if(navigator.userAgent.indexOf("Chrome")<0||loginPage)
						window.open(conextPath + "/jsp/common/main.jsp", "_blank", "width=" + window.screen.width + ",height=" + (window.screen.height - 20) + ",menubar=no,toolbar=no,location=no,top=0,left=0,directories=no,status=no,scrollbars=yes,resizable=no");
					else
						window.location.href=conextPath +"/jsp/common/main.jsp";			
			}else{
				if(_isIE){
					window.open(conextPath + "/jsp/myhome/myHome.jsp", "_blank", "width=" + window.screen.width + ",height=" + (window.screen.height - 90) + ",menubar=no,toolbar=no,location=no,top=0,left=0,directories=no,status=no,scrollbars=yes,resizable=no");	
				}else{
					if(navigator.userAgent.indexOf("Chrome")>0&&typeof(autoFlg) !='undefined')
						window.location.href=conextPath +"/jsp/myhome/myHome.jsp";	
					else
						window.open(conextPath + "/jsp/myhome/myHome.jsp", "_blank", "width=" + window.screen.width + ",height=" + (window.screen.height - 20) + ",menubar=no,toolbar=no,location=no,top=0,left=0,directories=no,status=no,scrollbars=yes,resizable=no");		
				 }  	
			}
				
		}
		
		function setCookie(obj, time){
			var date=new Date();
			date.setTime(date.getTime()+ 2592000000);
			document.cookie = obj + "; expires=" + date.toGMTString();
		}
		
		function getCookie(key){
			var strCookie = document.cookie;
			var arrCookie = strCookie.split("; ");
			var value = "";
			for(var i = 0; i < arrCookie.length; i++){
				var arr = arrCookie[i].split("=");
				if(key == arr[0]){
					value = arr[1];		
					break;
				}
			}
			return unescape(value);
		}
		
		function getkeyCode(e) {
		    var keynum = "";
		    if(_isIE) {
		        keynum = e.keyCode;
		    }else {
		        keynum = e.which;
		    }
		    return keynum;
		}
		
		function enter(obj,autoFlg){
			obj.onkeydown = function(e) {
		        if(_isIE) {
		            if(getkeyCode(event) == 13) {
						if(obj.id == "loginlName"){
							$("loginPwd").focus();
						}else if(obj.id == "loginPwd"){
							if($("loginButton").style.display=="none"){
								$("loginMessage").innerHTML = "请刷新当前页 再登录";return;
							}
							if("true"!=validCodeFlg){
								loginCheck(autoFlg);
							}else{
								$("viewCode").focus();
							}
						}else if(obj.id == "loginButton"||obj.id == "viewCode"){
							loginCheck(autoFlg);
						}
		            }
		        }else {
		            if(getkeyCode(e) == 13) {
		            	if(obj.id == "loginlName"){
							$("loginPwd").focus();
						}else if(obj.id == "loginPwd"){
							if($("loginButton").style.display=="none"){
								$("loginMessage").innerHTML = "请刷新当前页 再登录";return;
							}
							if("true"!=validCodeFlg && $("loginButton").style.display==""){
								loginCheck(autoFlg);
							}else{
								$("viewCode").focus();
							}
						}else if(obj.id == "loginButton"||obj.id == "viewCode"){
							loginCheck(autoFlg);
						}
		            }
		        }
	    	}
		}
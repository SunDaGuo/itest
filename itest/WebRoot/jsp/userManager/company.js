	var selectAllFlag = false;
	var jsons;
	var deleteFlag = 0;
	pageBreakUrl = conextPath+"/userManager/userManagerAction!adminList.action";
	var pageBreakForm = "";
	var pageSizec = 0;
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,公司名称,公司类型,公司规模,负责人,负责人帐号,负责人Mail,负责人电话,公司电话,传真,公司地址,网站,公司介绍,状态");
    pmGrid.setInitWidths("40,40,*,70,70,70,80,80,80,80,80,80,80,80,50");
    pmGrid.setColAlign("left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,int,str,str,str,str,str,str,str,str,str,str,str,str,str");
	//pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableTooltips("false,false,true,true,true,true,true,true,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
   	pmGrid.attachEvent("onRowSelect",doOnSelect);
    if($("listStr").value != ""){
    	var data = $("listStr").value;
    	data = data.replace(/[\r\n]/g, "");
	    var datas = data.split("$");
	    if(datas[1] != ""){
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setPageNoSizeCount(datas[0]);
	    	setRowNum();
	    }
    }
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
	}   
	function getChecked(GridObj){
		selectItems = "";
		var currGrid = pmGrid;
		if(typeof(GridObj)!="undefined"){
			currGrid = GridObj;
		}
		var allItems = currGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && currGrid.cells(items[i], 0).getValue() == 1){
				if (selectItems == ""){
					selectItems = items[i];
				}else{
					selectItems += "," + items[i];
				}
			}
		}
		return selectItems;
	}
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
	});
	
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			find("findD");
		}else if(id == "new"){
			creater(id, "createD");
		}else if(id == 'update'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要修改的记录");
			}else{
				creater(id, "createD");
			}
		}else if(id == 'delete'){
			MStatus();
		}else if(id == 'deleteR'){
			if(pmGrid.getSelectedId()== null){
				hintMsg("请选择要删除的记录");
			}else{
				cfDialog("deleteExe","您确定删除选择的记录?",false);
			}
		}else if(id == "first"){
			pageAction(1, pageSize);
		}else if(id == "last"){
			pageAction(pageCount, pageSize);
		}else if(id == "next"){
			pageAction(pageNo + 1, pageSize);
		}else if(id == "pervious"){
			pageAction(pageNo -1, pageSize);
		}else if(id == "hidden"){
			hideCol();
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
   function setPageNoSizeCount(pageStr){
		var pageNoSizeCount = pageStr.split("/");
		pageNo = parseInt(pageNoSizeCount[0]);
		pmBar.setValue("page", pageNo);
		pmBar.setItemText("pageP", pageNoSizeCount[1]);
		pageSize = parseInt(pageNoSizeCount[1])
		pageCount = parseInt(pageNoSizeCount[2]);
		pageStr =  "/" +pageCount +"页" ;
		pmBar.setItemText("pageMessage", pageStr);
		pmBar.setMaxValue("slider", pageCount, "");
		pmBar.setValue("slider", pageNo);
		if(pageNo == 1){
			pmBar.disableItem("first");
			pmBar.disableItem("pervious");  
			if(pageCount <= 1){
				pmBar.disableItem("next");
				pmBar.disableItem("last");
				pmBar.disableItem("slider");
			}else{
				pmBar.enableItem("next");
				pmBar.enableItem("last");
				pmBar.enableItem("slider");
			}
		}else if(pageNo == pageCount){
			pmBar.enableItem("first");
			pmBar.enableItem("pervious");
			pmBar.disableItem("next");
			pmBar.disableItem("last");
		}else if(pageNo > 1 && pageNo < pageCount){
			pmBar.enableItem("first");
			pmBar.enableItem("pervious");
			pmBar.enableItem("next");
			pmBar.enableItem("last");
		}else{
			pmBar.disableItem("first");
			pmBar.disableItem("pervious");
			pmBar.disableItem("next");
			pmBar.disableItem("last");
			pmBar.disableItem("slider");
		}
	}
    function setRowNum(){
        //用来标识，当前页超过每页显示记录后的记录ID 
    	var rowIdsToDel ="";
		for(var i = 0; i < pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,1).setValue((i + 1)+((parseInt(pageNo)-1)*parseInt(pageSize)));
			if(i>(pageSize-1)){
				if(rowIdsToDel==""){
					rowIdsToDel = pmGrid.getRowId(i);
				}else{
					rowIdsToDel = rowIdsToDel +"," +pmGrid.getRowId(i);
				}
			}
		}
		if(rowIdsToDel !=""){
			var ids = rowIdsToDel.split(",");
			for(var l=0; l <ids.length; l++){
			 pmGrid.deleteRow(ids[l]);
			}
		}
    }

    function doOnCheck(rowId,cellInd,state){//勾选
		pmGrid.setSelectedRow(rowId);
		return true;
	}
	function setRowIndex(obj, index){
		var rowC = obj.getRowsNum();
		for(var i = 0; i < rowC; i++){
			obj.cells2(i,index).setValue(i + 1);
		}
	}
	function MStatus(){
		var chgIds = pmGrid.getSelectedId();
		if(chgIds==null){
			hintMsg("请选择记录");
			return ;
		}else if(getChecked().indexOf(",")>0){
			hintMsg("请选择一条记录");
			return ;
		}
		cW_ch = initW_ch(cW_ch,  "chgStatus", true, 150, 80);
		cW_ch.setText("更改状态");
		$("chgeStatus").options[0].selected = true;
		cW_ch.setDimension(150, 80);
	}
	
	function eChgSts(){
		var statusV = $("chgeStatus").value;
		if(statusV ==-2 ||isWhitespace($("chgeStatus").value)){
		 	$("cmText").innerHTML = "请选择要更改的状态";
		 	return ;
		}
		var chgIds = pmGrid.getSelectedId();
		var url = conextPath+"/userManager/userManagerAction!chagCompStatus.action?dto.company.id="+chgIds +"&dto.company.status=" +statusV;
		var chgInfo = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
		if(chgInfo=="success"){
			var StatusStr = $("chgeStatus").options[$("chgeStatus").selectedIndex].text ;
			pmGrid.cells(chgIds,14).setValue(StatusStr);
			cW_ch.hide();
			cW_ch.setModal(false);		
		}else{
			$("cmText").innerHTML = "操作失败";
		}
	}
 	function adjustTable(tableId){
		var ctable=$(tableId);
		var count = 1;
		for(var i=0;i<ctable.rows.length;i++){
		   if(ctable.rows[i].style.display==""){
			   if((count%2)==0){
			   		ctable.rows[i].className="odd_mypm";
			   }else{
			   		ctable.rows[i].className="ev_mypm";
			   }
			   count++;
			  }
		}
	}
	function creater(id, divId){
		if(id=="new"){
			$("inVoldPwdR").style.display = "none";
			if(_isIE){
				cuW_ch = initW_ch(cuW_ch, divId, true, 470, 345);
			}else{
				cuW_ch = initW_ch(cuW_ch, divId, true, 470, 380);
			}
			$("loginName").readOnly=false;
		}else{
			$("questionR").style.display = "none";
			$("answerR").style.display = "none";
			$("pwdR").style.display = "none";
			if(_isIE){
				cuW_ch = initW_ch(cuW_ch, divId, true, 470, 280);
			}else{
				cuW_ch = initW_ch(cuW_ch, divId, true, 470, 255);
			}
			$("loginName").readOnly=true;
		}
		formReset("createF", "name");
		$("msgD").innerHTML = "&nbsp;";
		if(id=="new"){		
			cuW_ch.setText("新建管理帐户");
			$("userId").value = "";
			$("creat_b").value = " 确定 ";
			$("questionR").style.display = "";
			$("answerR").style.display = "";
			$("pwdR").style.display = "";
			adjustTable("createTable");
		}else if(id=="update"){
			cuW_ch.setText("修改管理帐户");
			var url = conextPath+"/userManager/userManagerAction!updateAdminInit.action?dto.company.id=";
			url += pmGrid.getSelectedId()+"&dto.isAjax=true";
			var adminInfo  = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
			adjustTable("createTable");
			if(adminInfo=="failed"){
			}else{
				setUpInfo(adminInfo);
			}
		}
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i <updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] !="null"){
				var valueStr ="$('"+currInfo[0]+"').value =currInfo[1]";
				eval(valueStr);	
			}
		}
		$("creat_b").value = " 修改 ";
	}
	function find(divId){
		fW_ch = initW_ch(fW_ch, divId, false, 460, 160);
		fW_ch.setText("查询");
		$("name_f").focus();
	}
	function deleteExe(){
		var url = conextPath+"/userManager/userManagerAction!deleteComp.action?dto.isAjax=true&dto.company.id=" + getChecked();
		var ajaxRest = dhtmlxAjax.postSync(url,"").xmlDoc.responseText;
		//alert(ajaxRest);
		if(ajaxRest== "success"){
			var dItems = deleteIds.split(',');
			for(var i = 0; i < dItems.length; i++){
				if(dItems[i] != "")
					pmGrid.deleteRow(dItems[i]);
			}
			dW_ch.hide();
			dW_ch.setModal(false);
			pmGrid.setSizes();		
		}else{
			hintMsg("执行删除发生错误");
		}
		
	}
	function checkCreateF(){
		if(isWhitespace($("name").value)){
			$("msgD").innerHTML = "请填写公司名称";
			$("name").focus();
			return;
		}else if($("companyType").value==0){
			$("msgD").innerHTML = "请选择公司类型";
			$("companyType").focus();
			return;		
		}else if($("companySize").value==0){
			$("msgD").innerHTML = "请选择公司规模";
			$("companySize").focus();
			return;		
		}else if(isWhitespace($("owner").value)){
			$("msgD").innerHTML = "请填写负责人姓名";
			$("owner").focus();
			return;		
		}else if(isWhitespace($("loginName").value)){
			$("msgD").innerHTML = "请填写登录帐号";
			$("loginName").focus();
			return;	
		}else if($("companyId").value =="" && isWhitespace($("password").value)){
			$("msgD").innerHTML = "请填写登录密码";
			$("password").focus();
			return;
		}else if($("companyId").value == "" && $("password").value != $("vpassword").value){
			$("msgD").innerHTML = "确认密码不正确";
			$("vpassword").focus();
			return;
		}else if(isWhitespace($("ownereMail").value)){
			$("msgD").innerHTML = "请填写负责人邮箱";
			$("ownereMail").focus();
			return;
		}
		var companyId = $("companyId").value ;
		var adminInfo = dhtmlxAjax.postSync(conextPath+"/userManager/userManagerAction!companyMaintence.action", "createF").xmlDoc.responseText;
		if(adminInfo.indexOf("^") != -1){
			var result = adminInfo.split("^");
			if(result[0] == "success"){
				var rowNum;
				if($("companyId").value == ""){
					rowNum = 0;
					pmGrid.addRow(result[1],result[2],0);
				}else{
					pmGrid.deleteRow(result[1]);
					pmGrid._addRow(result[1],result[2],0);
				}
				setRowNum();
				pmGrid.setSelectedRow(pmGrid.getRowId(0));
				cuW_ch.hide();
				cuW_ch.setModal(false);
				pmGrid.setSizes();
				return;
			}else{
				$("msgD").innerHTML =  "操作失败";
			}
		}else{
			$("msgD").innerHTML = "操作失败";
		}
		return;
	}
	function checkAll(){//全选
		if(!selectAllFlag)
			selectAllFlag = true;
		else
			selectAllFlag = false;
		pmBar.setItemImage('all', "ch_" + selectAllFlag + ".gif");
		var allItems = pmGrid.getAllItemIds();
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			if(items[i] != "" && pmGrid.getRowById(items[i]).style.display == "")
				pmGrid.cells(items[i],0).setValue(selectAllFlag);
		}
	}
	function efind(){
		var adminInfos = dhtmlxAjax.postSync(conextPath+"/userManager/userManagerAction!adminList.action?dto.isAjax=true", "findF").xmlDoc.responseText;
	   	var datas = adminInfos.split("$");
	    if(datas[1] != ""){
	    	pmGrid.clearAll();
	    	jsons = eval("(" + datas[1] +")");
	    	pmGrid.parse(jsons, "json");
	    	setRowNum();
	    	setPageNoSizeCount(datas[0]);
	    	pageBreakUrl = conextPath+"/userManager/userManagerAction!adminList.action";
	   		pageBreakForm = "findF";
	    }else{
	    	hintMsg("没查到相关记录");
	    }
	}
	
	function setDefault(obj){
		if(obj.value == 2 || obj.value == 3 || obj.value == 4 || obj.value == 5 || obj.value == 11 )
			obj.checked = true;
	}
	function isExistObject(objs, obj){
		var arr = objs.split(",");
		for(var i = 0; i < arr.length; i++){
			if(obj == arr[i]){
				$("h-" + obj).checked = true;
				return true;
			}
		}
		return false;
	}
	function pageAction(pageNo, pageSize){
		if(pageNo>pageCount && pageSizec<1){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var userJson = dhtmlxAjax.postSync(pageBreakUrl + "?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize, pageBreakForm).xmlDoc.responseText.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
		}else{
	   		pmGrid.clearAll();
	   		pmGrid.parse(eval("(" + userJson[1] +")"), "json");
	   		setPageNoSizeCount(userJson[0]);
	   		setRowNum();
   		}
   		pageSizec = 0;
	}
	function initW_ch(obj, divId, mode, w, h){//初始化 window
		if((typeof obj != "undefined") && !obj.isHidden()){
			obj.setDimension(w,h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.setDimension(w,h);
			obj.show();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			obj = cufmsW.createWindow(divId, 110, 110, w, h);
			if(divId != null)
				obj.attachObject(divId, false);
			hiddenB(obj, mode);
		}
		obj.setModal(mode);
		return obj;
	}
		
	function initCufmsW(){//初始化window
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.vp.style.border = "#909090 1px solid";
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
﻿<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>

<HTML>

	<HEAD>
		<TITLE>关于itest</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>

	</HEAD>
	<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
		bgcolor="#ffffff" style="text-align: center">
		<br>
		<center>
			<div
				style="margin: 0 auto; text-align: center; width: 685px; height: 260px; background: url(<%=request.getContextPath()%>/jsp/common/images/error_bj.gif );"></div>
			<div align="center" width="100%" style="margin-top: -200px;">
				<table width="100%" border="0" align="center" cellpadding="0"
					cellspacing="0">
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;&nbsp;&nbsp;Itest(爱测试)，最懂测试人的测试管理软件 
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;测试人自己开发，汇聚10年沉淀，独创流程驱动测试、度量展现测试人价值的测试协同软件。
							</font>
						</td>
					</tr>

					<tr height="10px">
					  <td></td>
					</tr>
					<tr>
						<td>
							<font color="red">
								&nbsp;&nbsp;&nbsp;itest采用GPL V2开源协议,当前版本为V2.5,也是mypm2.5版本 
							</font>
						</td>
					</tr>
					<tr>
						<td>
						</td>
					</tr>
					<tr height="10px">
					  <td></td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;&nbsp;
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp;
							</font>
						</td>
					</tr>

					<tr>
						<td>
							<font color="blue">
								&nbsp;&nbsp;&nbsp;官网 <a href="https://itest.work" target="_blank">itest.work</a> &nbsp;&nbsp;&nbsp;QQ群 797761290
							</font>
						</td>
					</tr>
					<tr>
						<td>
							<font color="blue">
								&nbsp; 
							</font>
						</td>
					</tr>																	
				</table>
			</div>
			</div>
		</center>
	</body>
	<script type="text/javascript" language="javascript">
	<ww:if test="#request.EXP_INFO.message=='overdue'"> 
        }else{
			window.parent.location="<%=request.getContextPath()%>/jsp/userManager/login.jsp";
		}
	</ww:if>  
</script>
</HTML>

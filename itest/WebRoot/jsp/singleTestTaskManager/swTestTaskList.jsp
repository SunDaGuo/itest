
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>测试项目</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>			
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY bgcolor="#ffffff">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="padding-right: 3px;">
					<div id="toolbarObj"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
		<ww:hidden id="reProStep" name="reProStep"></ww:hidden>
	<script type="text/javascript">
	<pmTag:button page="swTestTaskList" find="true" checkAll="false" back="false"/>
	var operCmdVar ="${dto.operCmd}";
	var taskIdField ="${dto.taskIdField}";
	var taskNameField ="${dto.taskNameField}";
	if(operCmdVar=="fromOutLine")
		ininPage("toolbarObj", "gridbox", 870);
	</script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/singleTestTaskManager/swTestTaskList.js"></script>
		<div id="findDiv"  class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="/singleTestTask" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" width="100%">
					<tr class="ev_mypm">
						<td colspan="4" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="odd_mypm" >
						<td align="right" class="rightM_center">项目编号:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proNum_f" name="dto.singleTest.proNum"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" class="rightM_center">项目名称:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:textfield id="proName_f" name="dto.singleTest.proName"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" class="rightM_center" style="border-right:0;">研发部门:</td>
						<td style="border-right:0;">
							<ww:textfield id="devDept_f" name="dto.singleTest.devDept"
								cssStyle="width:120;padding:2 0 0 4;" cssClass="text_c"></ww:textfield>
						</td>
						<td align="right" style="border-right:0;" class="rightM_center">状态:</td>
						<td style="border-right:0;" class="dataM_left">
							<ww:select id="status_f" name="dto.singleTest.status"
								list="#{-1:'',0:'进行',1:'完成',2:'结束',2:'准备'}" headerValue="-1" cssStyle="width:120;" cssClass="text_c">
							</ww:select>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" class="dataM_left" style="border-right:0;width:340">&nbsp;</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right">
							<a class="bluebtn" href="javascript:void(0);" id="quBtn"onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
							style="margin-left: 6px;"><span> 返回</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="quU_b"onclick="findExe();"
								style="margin-left: 6px;"><span> 查询</span> </a>
							<a class="bluebtn" href="javascript:void(0);" id="qretBtn"onclick="formReset('findForm');"
							style="margin-left: 6px;"><span> 重置</span> </a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4">&nbsp;</td>
					</tr>
				</table>
			</ww:form>
		</div>
		</div>
	<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>

</HTML>

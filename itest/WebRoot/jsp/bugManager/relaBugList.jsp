
<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>关连Bug</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
	</HEAD>
	<BODY  style="overflow-y:hidden;">
	<script type="text/javascript">
	
	<pmTag:priviChk urls="caseManagerAction!exeCase" varNames="canExe"/>
	var bugIdsInit = "${dto.bugIds}";
	var flwStr ="${dto.testFlow}";
	var roleStr ="${dto.roleInTask}";
	var isExeRela ="${dto.isExeRela}";
	function styleChange(num){
	 	 	switch(num){
				case 1:
					$("bugInfoTab").className="jihuo";
					$("baseInfoTab").className="nojihuo";
					loadBug();
					break;
				case 2:
					$("bugInfoTab").className="nojihuo";
					$("baseInfoTab").className="jihuo";
					 loadBaseInfo();
					break;
	 		}
	 } 
	 function loadBug(){
	 	$("bugListDiv").style.display="";
	 	$("baseInfoDiv").style.display="none";
	 	//parent.relBugW_ch.setDimension(850, 535);
	 }
	 var haveLoad = false;
	 function loadBaseInfo(){
	 	if(haveLoad){
			$("baseInfoDiv").style.display="";	 
	 		$("bugListDiv").style.display="none";	
	 		//parent.relBugW_ch.setDimension(850, 500);
	 		return;	 	
	 	}
		var url = url = conextPath+"/caseManager/caseManagerAction!viewDetal.action?dto.isAjax=true&dto.testCaseInfo.testCaseId="+$("testCaseId").value;
		var ajaxRest = postSub(url,"");	 
		if(ajaxRest=="failed"){
			hintMsg('加载基本信息失败');
			return;
		}
		//parent.relBugW_ch.setDimension(850, relBugW_ch);
		haveLoad=true;
		ajaxRest = ajaxRest.replace(/[\r\n]/g, "");
		setBaseInfo(ajaxRest);
		$("baseInfoDiv").style.display="";	 
	 	$("bugListDiv").style.display="none";
	 	loadCaseFCK();	
	}
	</script>
		<div align="center" id="tabSw">
			<table border="0" style="width:100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="jihuo" id="bugInfoTab" width="80" class="tdtxt"
						align="right">
						<strong><a onclick="styleChange(1)"
							href="javascript:void(0);">BUG信息</a> </strong>
					</td>
					<td class="nojihuo" id="baseInfoTab" width="60" class="tdtxt"
						align="left">
						<strong><a onclick="styleChange(2)"
							href="javascript:void(0);">基本信息</a> </strong>
					</td>
					<td colspan="6" width="560">
						&nbsp;
					</td>
				</tr>
			</table>
		</div>
		<div id="bugListDiv" align="center"  style="background-color: #ffffff; width:100%;">
			<input type="hidden" id="listStr" name="listStr" value="${dto.listStr}"/>
			<ww:hidden id="bugIds" name="dto.bugIds"></ww:hidden>
			<ww:hidden id="testCaseId" name="dto.testCaseId"></ww:hidden>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td valign="top">
						<div id="toolbarObj"></div>
					</td>
				</tr>
				<tr>
					<td valign="top">
						<div id="gridbox"></div>
					</td>
				</tr>
				<tr>
					<td class="tdtxt" align="center" width="720" colspan="6">
						<a class="bluebtn"
							href="javascript:parent.relBugW_ch.setModal(false);parent.relBugW_ch.hide();"
							style="margin-left: 6px"><span> 返回</span>
						</a>
						<a class="bluebtn" id="saveRela"href="javascript:relaBug();"
							style="margin-left: 6px;display:none""><span> 关联</span>
						 </a>
						<a class="bluebtn" id="upRela"href="javascript:relaBug();"
							style="margin-left: 6px;display:none"><span> 更新关联</span>
						 </a>
					</td>
				</tr>
			</table>
		</div>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:hidden id="typeSelStr" name="dto.bug.typeSelStr" ></ww:hidden>
			<ww:hidden id="gradeSelStr" name="dto.bug.gradeSelStr" ></ww:hidden>
			<ww:hidden id="freqSelStr" name="dto.bug.freqSelStr" ></ww:hidden>
			<ww:hidden id="occaSelStr" name="dto.bug.occaSelStr" ></ww:hidden>
			<ww:hidden id="geCaseSelStr" name="dto.bug.geCaseSelStr" ></ww:hidden>
			<ww:hidden id="sourceSelStr" name="dto.bug.sourceSelStr" ></ww:hidden>
			<ww:hidden id="plantFormSelStr" name="dto.bug.plantFormSelStr"></ww:hidden>
			<ww:hidden id="genePhaseSelStr" name="dto.bug.genePhaseSelStr"></ww:hidden>
			<ww:hidden id="priSelStr" name="dto.bug.priSelStr"></ww:hidden>
			<input type="hidden" id="testSelStr" name="testSelStr" value=""/>
			<input type="hidden" id="devStr" name="devStr" value=""/>
			<input type="hidden" id="assignSelStr" name="assignSelStr" value=""/>
			<input type="hidden" id="analySelStr" name="analySelStr" value=""/>
			<input type="hidden" id="verSelStr" name="verSelStr" value=""/>
			<input type="hidden" id="interCesSelStr" name="interCesSelStr"value=""/>
			<input type="hidden" id="devChkIdSelStr" name="devChkIdSelStr" value=""/>
			<input type="hidden" id="testChkIdSelStr" name="testChkIdSelStr" value=""/>
			<input type="hidden" id="testLdIdSelStr" name="testLdIdSelStr"value=""/>
			<input type="hidden" id="roleInTask" name="roleInTask" value=""/>
			<input type="hidden" id="testFlow" name="testFlow" value=""/>
			<input type="hidden" id="loadType" name="loadType" value=""/>
			<input type="hidden" id="querySelStr" name="querySelStr" value=""/>
			<input type="hidden" id="queryCount" name="queryCount" value=""/>
			<ww:hidden id="testPhase" name="dto.bug.testPhase"></ww:hidden>
			<input type="hidden" id="nodeDataStr" name="nodeDataStr" value=""/>
			<input type="hidden" id="relCaseSwitch" name="relCaseSwitch" value=""/>
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
				<ww:hidden id="bugModuleId" name="dto.bug.moduleId" ></ww:hidden>
				<ww:hidden id="bugId" name="dto.bug.bugId"></ww:hidden>
				<ww:hidden id="reProTxt" name="dto.bug.reProTxt"></ww:hidden>
				<ww:hidden id="currStateId" name="dto.bug.currStateId"></ww:hidden>
				<ww:hidden id="nextFlowCd" name="dto.bug.nextFlowCd"></ww:hidden>
				<ww:hidden id="currFlowCd" name="dto.bug.currFlowCd"></ww:hidden>
				<ww:hidden id="nextOwnerId" name="dto.bug.nextOwnerId"></ww:hidden>
				<ww:hidden id="moduleNames" name="dto.moduleName"></ww:hidden>
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="100%">
					<tr>
						<td width="80" class="rightM_center" align="right" style="border-right:0">
							不重置:<input id="clearUpFlg" name ="clearUpFlg" type="checkbox" value="1" onclick="setClearFlg(this)" checked="true"/>	
						</td>					
						<td colspan="5" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" style="color:red;border-right:0">
							测试需求:
						</td>
						<td class="tdtxt" align="left" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="mdPath" name="dto.mdPath" readonly="true"
								cssStyle="width:640;padding:2 0 0 4;" cssClass="text_c"
								  ></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80"  class="rightM_center" align="right" style="border-right:0">
							状态:
						</td>
						<td class="tdtxt" align="left" colspan="3" width="400"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="stateName" name="stateName" readonly="true" cssClass="text_c"
								cssStyle="width:160;padding:2 0 0 4;"></ww:textfield>
						</td>
						<td width="80"  class="rightM_center" align="right" style="color:red;border-right:0">
							发现版本:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0" nowrap>
							<ww:hidden id="bugReptVer" name="dto.bug.bugReptVer"
								onkeypress="javascript:return;"></ww:hidden>
							<ww:textfield id="bugReptVerName" name="dto.bug.reptVersion.versionNum"
								cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c"
								  readonly="true"
								onfocus="popSelWin('bugReptVer','bugReptVerName','verSelStr','发现版本')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							Bug描述:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="bugDesc" name="dto.bug.bugDesc" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							类型:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugTypeId" name="dto.bug.bugTypeId"
								onkeypress="javascript:return;"></ww:hidden>
							<ww:textfield id="bugTypeName" name="dto.bug.dtoHelper.bugType.typeName"
								cssStyle="width:160;padding:2 0 0 4;" cssClass="text_c"
								  readonly="true"
								onfocus="popSelWin('bugTypeId','bugTypeName','typeSelStr','类型')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							等级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugGradeId" name="dto.bug.bugGradeId"></ww:hidden>
							<ww:textfield id="bugGradeName" name="dto.bug.dtoHelper.bugGrade.typeName"
								cssStyle="width:160;padding:2 0 0 4;"
								  readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugGradeId','bugGradeName','gradeSelStr','等级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							平台:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="platformId" name="dto.bug.platformId"></ww:hidden>
							<ww:textfield id="pltfomName" name="dto.bug.dtoHelper.occurPlant.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('platformId','pltfomName','plantFormSelStr','发生平台')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							来源:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="sourceId" name="dto.bug.sourceId"></ww:hidden>
							<ww:textfield id="sourceName" name="dto.bug.dtoHelper.bugSource.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('sourceId','sourceName','sourceSelStr','来源')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							发现时机:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugOccaId" name="dto.bug.bugOccaId"></ww:hidden>
							<ww:textfield id="occaName" name="dto.bug.dtoHelper.bugOpotunity.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugOccaId','occaName','occaSelStr','发现时机')"></ww:textfield>
						</td>
						<td width="80"  class="rightM_center" align="right" class="rightM_center" 
							style="color:red;border-right:0">
							<div id="geneCauseTd"style="display: none;">测试时机:</div>
						</td>
						<td class="tdtxt"  width="160"
							style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="geneCauseId" name="dto.bug.geneCauseId"></ww:hidden>
							<div id="geneCauseF" style="display: none;">
							<ww:textfield id="geneCaseName" name="dto.bug.dtoHelper.geneCause.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('geneCauseId','geneCaseName','geCaseSelStr','测试时机')">
							</ww:textfield>
							</div>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							优先级:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugPriId" name="dto.bug.priId"></ww:hidden>
							<ww:textfield id="priName" name="dto.bug.dtoHelper.bugPri.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugPriId','priName','priSelStr','优先级')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							频率:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="bugFreqId" name="dto.bug.bugFreqId"></ww:hidden>
							<ww:textfield id="bugFreqName" name="dto.bug.dtoHelper.bugFreq.typeName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('bugFreqId','bugFreqName','freqSelStr','频率')"></ww:textfield>
						</td>
						<td width="80" class="rightM_center" align="right"
							 class="rightM_center" style="color:red;border-right:0">
							<div  id="repropTd" style="color:red;display: none">再现比例:</div>
						</td>
						<td  class="tdtxt" align="left"
							style="padding: 2 0 0 4; width: 160;border-right:0">
							<div id="reprop" style="display: none;">
							<ww:textfield id="reproPersent" name="dto.bug.reproPersent" cssClass="text_c"
								cssStyle="width:160;padding:2 0 0 4;"></ww:textfield>
							</div>
						</td>
					</tr>
					<tr style="display:none">
						<td id="impPhaTdtxt" width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;display: none;border-right:0">
							引入原因:
						</td>
						<td id="impPhaTd" class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4; display: none;border-right:0">
							<ww:hidden id="genePhaseId" name="dto.bug.genePhaseId"></ww:hidden>
							<ww:textfield id="genPhName" name="genPhName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('genePhaseId','genPhName','genePhaseSelStr','引入原因')"></ww:textfield>
						</td>
					</tr>
					<tr>
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" nowrap>
							再现步骤:
						</td>
						<td class="tdtxt" colspan="5"
							style="background-color: #f7f7f7; hight: 200; width: 640;border-right:0">
							<textarea name="dto.bug.reProStep" id="reProStep" cols="50"
								rows="25" style="width:640;hight:200;padding:2 0 0 4;">
   			        		  <ww:property value="dto.bug.reProStep" escape="false" />
   			                </textarea>
						</td>
					</tr>
					<tr id="testOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转互验人:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="testOwnerId" name="dto.bug.testOwnerId"></ww:hidden>
							<ww:textfield id="testOwnName" name="testOwnName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('testOwnerId','testOwnName','testSelStr','互验人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="analOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转分析人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="analyseOwnerId" name="dto.bug.analyseOwnerId"></ww:hidden>
							<ww:textfield id="analOwnerName" name="analOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('analyseOwnerId','analOwnerName','analySelStr','分析人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					<tr id="anasnOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转分配人:
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="assinOwnerId" name="dto.bug.assinOwnerId"></ww:hidden>
							<ww:textfield id="anasnOwnerName" name="anasnOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('assinOwnerId','anasnOwnerName','assignSelStr','分配人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" id="anaAssFromMdTd" width="480" colspan="4" style="display: none;border-right:0">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson(3)"><span>从测试需求指定人员中指派</span> </a>
						</td>
					</tr>
					<tr id="devOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转修改人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="devOwnerId" name="dto.bug.devOwnerId"></ww:hidden>
							<input type="hidden" id="moduleDevStr" name="moduleDevStr"
								value="" />
							<ww:textfield id="devOwnerName" name="dto.bug.dtoHelper.devOwner.uniqueName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('devOwnerId','devOwnerName','devStr','修改人')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" id="assFromMdTd" width="480" colspan="4" style="border-right:0;display: none;">
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson(1)"><span>从测试需求指定人员中指派</span> </a>
						</td>
					</tr>
					<tr id="intecsOwnerTr" style="display: none;">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							转仲裁人 :
						</td>
						<td class="tdtxt" width="160" style="padding: 2 0 0 4;border-right:0">
							<ww:hidden id="intercessOwnerId" name="dto.bug.intercessOwnerId"></ww:hidden>
							<ww:textfield id="intecsOwnerName" name="intecsOwnerName"
								cssStyle="width:160;padding:2 0 0 4;" readonly="true" cssClass="text_c"
								onfocus="popSelWin('intercessOwnerId','intecsOwnerName','devStr','仲裁')"></ww:textfield>
						</td>
						<td class="tdtxt" align="left" width="480" colspan="4" style="display: none;border-right:0" >
							<a class="graybtn" href="javascript:void(0);" onclick="getMdPerson()"><span>从测试需求开发人员中指派</span> </a>
						</td>
					</tr>
					</ww:form>
				    <ww:form enctype="multipart/form-data" theme="simple" name="fileform" id="fileform" action="" method="post" target="target_upload">
					  <tr class="odd_mypm">
					  	 <td class="rightM_center" width="90" id="attachTd" style="border-right:0">
					  	 附件/插图片:
					  	 </td>
					    <td class="dataM_left"  colspan="5" width="630" style="border-right:0">
							<input name="currUpFile" id="currUpFile" type="file" style="padding:2 0 0 4;width:210" Class="text_c">
							<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
							<img src="<%=request.getContextPath()%>/dhtmlx/toolbar/images/img_insert.png" id="insertImg" alt="当前位置插入图片" title="当前位置插入图片" onclick="upLoadAndSub('newSub','addSubCheck',1,oEditor);" />
					    </td>
					  </tr>	
					  <tr class="ev_mypm">
					    <td width="90" align="right"  style="border-right:0">&nbsp;</td>
						<td width="630" style="width:200;padding:2 0 0 4;border-right:0" colspan="5"><div id="upStatusBar"></div></td>		  
					  </tr>		
				  </ww:form>	
 		  	      <iframe id="target_upload" name="target_upload" src="" frameborder="0" scrolling="no" width="0" height="0"></iframe>
					<tr>
					<td class="tdtxt" align="center" width="720" colspan="6">
					  <a class="bluebtn" href="javascript:reSetFlg=0;cuW_ch.setModal(false);cuW_ch.hide();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtnc" onclick="saveBtnId='saveBtnc';contin='1';upLoadAndSub('newSub','addSubCheck',0,oEditor);" style="margin-left: 6px"><span>确定并继续</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn"onclick="contin='';upLoadAndSub('newSub','addSubCheck',0,oEditor);" style="margin-left: 6px"><span>确定</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="reSetAttaBtn" onclick="$('currUpFile').value='';if(_isIE)$('currUpFile').outerHTML=$('currUpFile').outerHTML" ><span>取消选取的文件</span> </a>
					<td>
					</tr>
				</table>
			</div>
		</div>
	<script type="text/javascript"src="<%=request.getContextPath()%>/jsp/bugManager/relaBug.js"></script>
	<script type="text/javascript">
	pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("new",0 , "", "new.gif");
	pmBar.setItemToolTip("new", "新增Bug");
	pmBar.addButton("first", 1, "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",2, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",3, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",4, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",5, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",6, "", 25);
	pmBar.addText("pageMessage",7, "");
	pmBar.addText("pageSizeText",8, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'));
	pmBar.addButtonSelect("pageP",9, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数")
	pmBar.addText("pageSizeTextEnd",10, "条");
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");

	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("选择,编号,Bug描述,状态,grade,fre,occi,btype,pri,测试人员,dev,报告日期,msgFlag,realCase,moduleId,bugReptId,currFlowCd,currHandlerId,currStateId,nextFlowCd,bugReptId");
    pmGrid.setInitWidths("40,80,*,80,0,0,0,0,0,80,0,125,0,0,0,0,0,0,0,0,0");
    pmGrid.setColAlign("center,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left,left");
    pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str");
	pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
	pmGrid.enableRowsHover(true, "red");
    pmGrid.init();
    pmGrid.enableTooltips("false,true,true,true,false,false,false,false,false,true,true,true,false,false,false,false,false,false,false,false,false");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");
    pmGrid.attachEvent("onRowSelect",doOnSelect); 
    pmGrid.attachEvent("onCheck",doOnChecks);
    function doOnSelect(rowId,index){
		this.setSelectedRow(rowId);
	}	
    function doOnChecks(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		if($("bugIds").value!=""){
			if(state){
				$("bugIds").value+=" "+rowId;
			}else{
				var idTmp = $("bugIds").value.split(rowId);
				$("bugIds").value =idTmp[0].replace(/\s+$|^\s+/g,"")+" "+idTmp[1].replace(/\s+$|^\s+/g,"");
			}			
		}else if($("bugIds").value==""&&state){
			$("bugIds").value=rowId;
		}
		return true;
	}  
	if($("bugIds").value==""){
		$("saveRela").style.display="";
		$("upRela").style.display="none";
	}else{
		$("saveRela").style.display="none";
		$("upRela").style.display="";	
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,1).cell.innerHTML="<a href='javascript:viewDetl()' title='查看明细'>"+getTttle2(i,1)+"</a>";
			if($("bugIds").value!=""&&$("bugIds").value.indexOf(pmGrid.getRowId(i))>=0){
				pmGrid.cells(pmGrid.getRowId(i),0).setValue(true);
			}
		}
		sw2Link();
	}
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
		
	}
	function colTypeReset(){
		pmGrid.setColTypes("ch,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ch,link,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
  
	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "new"){
			addInit();
		}else if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1"){
			if(pageSize==10){
				return;
			}
			pageSize = 10;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id2"){
			if(pageSize==15){
				return;
			}
			pageSize = 15;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id3"){
			if(pageSize==20){
				return;
			}
			pageSize = 20;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id4"){
			if(pageSize==25){
				return;
			}
			pageSize = 25;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "id5"){
			if(pageSize==30){
				return;
			}
			pageSize = 30;
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}
	});

	function pageAction(pageNo, pSize){
		if(pageNo>pageCount){
			pmBar.setValue("page", pageNo);
			return ;
		}
		purl = conextPath+"/bugManager/relaCaseAction!loadRelaBug.action?dto.isAjax=true&dto.moduleId="+$("moduleIdTmp").value+"&dto.testCaseId="+$("testCaseId").value +"&dto.pageNo="+ pageNo+"&dto.pageSize=" + pSize;
		var ajaxRest = postSub(purl,"");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
		}
		$("listStr").value=ajaxRest;
		colTypeReset();
		initGrid(pmGrid,"listStr",pmBar,"noSetRowNum");	
		loadLink();
   		return;
	}
	if(navigator.userAgent.indexOf("Chrome")>0){
		cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 530);
	}else
		cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 470);
	cuW_ch.setText("新增软件问题报告");
	cuW_ch.setModal(false);
	cuW_ch.hide();
	var reSetFlg=1,oEditor;
	function addInit(){
		if(reSetFlg==1){
			if(getCookie("clearFlg")==0){
				$("bugDesc").value="";			
			}else{
				$('createForm').reset();
				$("clearUpFlg").checked=false;
				disReset();
				resetHdeField();
				upVarReset();
			}
		}
		var url="";
		if($("typeSelStr").value==""){
			url=conextPath +"/bugManager/bugManagerAction!addInit.action?dto.isAjax=true&&dto.loadType=1";
		}else{
			url=conextPath +"/bugManager/bugManagerAction!addInit.action?dto.isAjax=true&&dto.loadType=0";
		}
		var ajaxResut = postSub(url,"");
		if(ajaxResut=="failed"){
			hintMsg('初始化失败');
			return false;
		}
		setInitInfo(ajaxResut);
		if(navigator.userAgent.indexOf("Chrome")>0){
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 530);
		}else
			cuW_ch=initW_ch(cuW_ch,  "createDiv", true,750, 500);
		cuW_ch.button("close").attachEvent("onClick", function(){
			eraseAttach('eraseAllImg');
			cuW_ch.setModal(false);
			cuW_ch.hide();
		});
		cuW_ch.setText("新增软件问题报告");
		loadFCK();
		if(getCookie("clearFlg")==0){
			try{oEditor.SetData("<strong>"+$("moduleName").value +"</strong>: ");}catch(err){}
		}
		cuW_ch.bringToTop();
		cuW_ch.setModal(true);
		$("bugDesc").focus();
		opreType="add";
		adjustTable("createTable");
	}
	var caseEditor,fckLoadName,pmEditor,pmEditor1;
	var fckHold= new Array();
	function loadFCK(){
		fckLoadName = "bug";
		if(typeof oEditor != "undefined"){
			if(reSetFlg==1)
				oEditor.SetData("<strong>"+$("mdPath").value +"</strong>:");
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	pmEditor = new FCKeditor('reProStep') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/";
    	pmEditor.Height = 200;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ReplaceTextarea();
    	return;
	}
	
	function loadCaseFCK(){
		fckLoadName = "caseFlg";
		if(typeof caseEditor != "undefined"){
			caseEditor.SetData($("initOpd").value);
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	pmEditor1 = new FCKeditor('operDataRichText') ;
    	pmEditor1.BasePath = conextPath+"/pmEditor/";
    	pmEditor1.Height = 270;
    	pmEditor1.ToolbarSet = "Basic" ; 
    	pmEditor1.ReplaceTextarea();
    	return;
	}
	function FCKeditor_OnComplete(editorInstance){
		fckHold[editorInstance.Name] = editorInstance;
		if(fckLoadName=="bug"){	
			oEditor = getFckInst('reProStep') ;
			if(reSetFlg==1)
				oEditor.SetData("<strong>"+$("mdPath").value +"</strong>:");
		}else{
			caseEditor = getFckInst('operDataRichText') ;
			caseEditor.SetData($("initOpd").value);				
		}
		return;
	}
	function getFckInst(idname){
		return fckHold[idname];
	}
	  </script>
		<div id="baseInfoDiv"  align="center" class="cycleTask gridbox_light" style="border:0px;display:none">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:hidden id="moduleIdTmp" name="dto.moduleId"></ww:hidden>
			<input type="hidden" id="initOpd" name="initOpd"/>
	  		<ww:hidden id="isReview" name="dto.isReview"></ww:hidden>
	  		<ww:hidden id="isTestLeader" name="dto.isTestLeader"></ww:hidden>
			<ww:form theme="simple" method="post" id="baseForm" enctype="multipart/form-data"
				name="baseForm" namespace="" action="">
				<ww:hidden id="bugId" name="dto.bugId"></ww:hidden>
				<ww:hidden id="testCaseIds" name="dto.testCaseIds"></ww:hidden>
			    <ww:hidden id="moduleId" name="dto.testCaseInfo.moduleId"></ww:hidden>
			    <ww:hidden id="createrId" name="dto.testCaseInfo.createrId"></ww:hidden>
			    <ww:hidden id="testCaseId" name="dto.testCaseInfo.testCaseId"></ww:hidden>
			    <ww:hidden id="isReleased" name="dto.testCaseInfo.isReleased"></ww:hidden>
				<ww:hidden id="creatdate" name="dto.testCaseInfo.creatdate"></ww:hidden>
				<ww:hidden id="auditId" name="dto.testCaseInfo.auditId"></ww:hidden>
			    <ww:hidden id="testStatus" name="dto.testCaseInfo.testStatus"></ww:hidden>
			    <ww:hidden id="testData" name="dto.testCaseInfo.testData"></ww:hidden>
			    <ww:hidden id="moduleNum" name="dto.testCaseInfo.moduleNum"></ww:hidden>
			    <ww:hidden id="attachUrl" name="dto.testCaseInfo.attachUrl"></ww:hidden>
			    <ww:hidden id="remark" name="dto.testCaseInfo.remark"></ww:hidden>
				<ww:hidden id="taskId" name="dto.testCaseInfo.taskId" value=""></ww:hidden>
			    <ww:hidden id="expResultOld" ></ww:hidden>
    			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
    			  <tr class="odd_mypm">
    			    <td width="50" class="rightM_center" style="color:red;">类型:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="caseTypeId" name="dto.testCaseInfo.caseTypeId"  Class="text_c"style="width:75" >
    			       <option   value="-1">请选择</option> 	        
    			      </select>
    			    </td>
    			    <td  width="50" class="rightM_center" style="color:red;" nowrap>优先级:</td>
    			    <td  class="dataM_left" width="75">
    			      <select id="priId" name="dto.testCaseInfo.priId" style="width: 75"  Class="text_c">
    			       <option   value="-1">请选择</option> 
    			      </select>
    			    </td>
    			    <td  class="rightM_center" nowrap>&nbsp;执行成本:
    			       <ww:textfield id="weight" name="dto.testCaseInfo.weight" cssStyle="width:70;padding:2 0 0 4;"  cssClass="text_c" onkeypress="javascript:return numChk(event);"></ww:textfield>
    			       <font color="Blue"  selected="true">一个成本单位代表5分钟</font> 
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >用例描述:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <ww:textfield id="testCaseDes" cssClass="text_c" readonly="true" name="dto.testCaseInfo.testCaseDes" cssStyle="width:560;padding:2 0 0 4;" ></ww:textfield>
    			    </td>
    			  </tr>
    			  <tr class="odd_mypm">
    			    <td width="90" class="rightM_center" style="color:red;" >过程及数据:</td>
    			    <td width="570" class="dataM_left"  Class="text_c" colspan="4" style="background-color: #f7f7f7;">
    			      <textarea name="dto.testCaseInfo.operDataRichText" id="operDataRichText" cols="50" rows="22" style="width:570;padding:2 0 0 4;">
    			        <ww:property value="dto.testCaseInfo.operDataRichText" escape="false"/>
    			      </textarea>
    			    </td>
    			  </tr>
    			  <tr class="ev_mypm">
    			    <td width="90" class="rightM_center" align="right">预期结果:</td>
    			    <td  class="dataM_left" colspan="4">
    			      <textarea id="expResult"  Class="text_c"  name="dto.testCaseInfo.expResult" onMouseOver="select()" onblur="javascript:if(this.value==''){this.value=$('expResultOld').value;}"  style="width:570;padding:2 0 0 4;" cols="100" rows="3">
    			      </textarea>
    			    </td>
    			  </tr>
				  <tr class="odd_mypm">
				  	 <td class="rightM_center" width="90" id="attachTd">
				  	 附件:
				  	 </td>
				    <td class="dataM_left"  colspan="4" width="570" style="border-right:0">
						<img src="<%=request.getContextPath()%>/images/button/attach.gif" style="display:none"id="currAttach2" alt="当前附件" title="打开当前附件" onclick="openAtta()" />
				    </td>
				  </tr>	
					<tr class="ev_mypm">
						<td class="tdtxt" align="left" width="650" colspan="5" style="border-right:0">
							<a class="bluebtn"href="javascript:parent.relBugW_ch.setModal(false);parent.relBugW_ch.hide();"
							style="margin-left: 6px"><span> 返回</span>
						    </a>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td class="tdtxt" align="left"  width="650" colspan="5" style="border-right:0"></td>
					</tr>
    			</table>
    		</ww:form>
    		</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
		<ww:include value="/jsp/common/downLoad.jsp"></ww:include>
		<script type="text/javascript">
		importJs(conextPath+"/jsp/common/upload.js");
		$("bugModuleId").value  = $("moduleIdTmp").value;
    	var clearFlg = getCookie("clearFlg");
    	if(typeof(clearFlg) =="undefined" || clearFlg == "" ||clearFlg=="undefined"){
    		setCookie("clearFlg=0", 2592000000);
    	}else if(clearFlg=="1"){
    		$("clearUpFlg").checked=false;
    	}
		</script>
	</BODY>
</HTML>

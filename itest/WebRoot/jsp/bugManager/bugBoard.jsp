<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<html>
	<head>
		<title>BUG面板</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
	</head>
	<body bgcolor="#ffffff" style="overflow-x:hidden;">
		<ww:hidden id="currTaskId" name="dto.loadType" ></ww:hidden>
		<table width="380" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top">
					<div id="toolbarObj"></div>
				</td>
			</tr>		
			<tr>
				<td valign="top">
					<div id="gridbox"></div>
				</td>
			</tr>
		</table>
	<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<script type="text/javascript">
	//ininPage("toolbarObj", "gridbox", 370);
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	//最后一隐藏列为是否发布的标识,和模块 id
    pmGrid.setHeader("&nbsp;,&nbsp;,项目人员,待处理BUG数,今日处理BUG次数,&nbsp;");
    pmGrid.setInitWidths("0,0,130,100,130,0");
    pmGrid.setColAlign("center,left,left,left,left,left");
    pmGrid.setColTypes("ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("str,int,str,int,int,str");
    pmGrid.enableTooltips("false,true,true,true,true,false");
    pmGrid.setSkin("light");
    pmGrid.enableAutoHeight(true, 300);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
	var data = "${dto.listStr}";
	if(data != ""){
		data = data.replace(/[\r\n]/g, "");
		pmGrid.clearAll();
		jsons = eval("(" + data +")");
		pmGrid.parse(jsons, "json");
	}	
	data = "";
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	}
	loadLink();
	function loadLink(){
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,3).cell.innerHTML="<a href=\"javascript:go2BugList('"+getTttle2(i,5)+"','"+getTttle2(i,3)+"')\" title='查看"+getTttle2(i,2)+"待处理BUG'>"+getTttle2(i,3)+"</a>";
		}
		pmGrid.setColTypes("ro,ro,ro,link,ro");
	}
	function go2BugList(lognVal,countVal){
		if(countVal=="0"){
			hintMsg("待处理BUG为0");
			return;
		}
		var url = conextPath+ "/bugManager/bugManagerAction!loadOwnerBug.action?dto.loadType="+$("currTaskId").value +"&dto.currOwner="+lognVal;
		parent.location= url;
	}
	</script>
	</BODY>
</HTML>

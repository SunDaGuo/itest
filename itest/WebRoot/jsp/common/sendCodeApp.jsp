<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
	<HEAD>
		<TITLE>激活管理</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css'href='<%=request.getContextPath()%>/css/page.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>			
	</HEAD>
	<BODY  style="overflow-x:hidden;overflow-y:hidden;">
	<script type="text/javascript">
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;">
		  <div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="codeAppForm"name="codeAppForm" namespace="" action="">
				<ww:hidden id="operCmd" name="dto.operCmd"></ww:hidden>
				<table border="0" id="createTable" class="obj row20px" cellspacing="0" align="center" cellpadding="0" border="0" width="720">
					<tr class="ev_mypm"> 
						<td colspan="6" class="tdtxt" align="center" width="720" style="border-right:0">
							<div id="cUMTxt" align="center"
								style="color: Blue"></div>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0">
							机器码:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="machineCode" name="dto.dto.machineCode" cssClass="text_c" readonly="true"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0">
							识别码:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="markCode" name="dto.dto.markCode" cssClass="text_c" readonly="true"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0">
							注册用数:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="currUseCount" name="dto.dto.currUseCount" cssClass="text_c" readonly="true"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" class="rightM_center" align="right" class="rightM_center" style="border-right:0">
							在线用户数:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="color:red;padding: 2 0 0 4;border-right:0">
							<ww:textfield id="useCount" name="dto.dto.useCount" cssClass="text_c" 
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80" onkeypress="javascript:return numChk(event);"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80" nowrap class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							公司名称:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="companyName" name="dto.dto.companyName" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="120"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80"  class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0" >
							公司网址:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="comWebSite" name="dto.dto.comWebSite" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="120"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80" nowrap  class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							公司地址:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="comAddress" name="dto.dto.comAddress" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="120"></ww:textfield>
						</td>
					</tr>	
					<tr class="odd_mypm">
						<td  width="80" class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							人员规模:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="comSize" name="dto.dto.comSize" cssClass="text_c" 
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80" onkeypress="javascript:return numChk(event);"></ww:textfield>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td width="80" nowrap class="rightM_center" align="right" class="rightM_center" style="border-right:0">
							联系人:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="owner" name="dto.dto.owner" cssClass="text_c" readonly="true"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="120"></ww:textfield>
						</td>
					</tr>
					<tr class="odd_mypm">
						<td width="80"  class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							联系电话:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="telephone" name="dto.dto.telephone" cssClass="text_c"
								cssStyle="width:640;padding:2 0 0 4;" maxlength="80"></ww:textfield>
						</td>
					</tr>		
					<tr class="ev_mypm">
						<td width="80"  class="rightM_center" align="right" class="rightM_center" style="color:red;border-right:0">
							电子邮箱:
						</td>
						<td class="tdtxt" colspan="5" width="640"
							style="padding: 2 0 0 4;border-right:0">
							<ww:textfield id="mailAddress" name="dto.dto.mailAddress" cssClass="text_c" 
								cssStyle="width:640;padding:2 0 0 4;" maxlength="120" ></ww:textfield>
						</td>
					</tr>																			
					<tr  class="ev_mypm">
					<td class="tdtxt" align="center" width="720" colspan="6" style="border-right:0">
					  <a class="bluebtn" href="javascript:void(0);"onclick="parent.parent.ulAppW_ch.setModal(false);parent.parent.ulAppW_ch.hide();"style="margin-left: 6px"><span> 返回</span> </a>
					  <a class="bluebtn" href="javascript:void(0);" id="saveBtn" onclick="sendApp();"><span>发送</span> </a>
					<td>
					</tr>
			</table>
		</ww:form>
		</div>
		</div>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	<script type="text/javascript">
	function sendApp(){
		if(!sendSubCheck())
			return;
		var opera = "userLicenseMgr";
		if($("operCmd").value=="sendReptLicenseInfo"){
			opera = "reptLicenseMgr";
		}
		var url = conextPath+"/commonAction!"+opera+".action";
		var rest = dhtmlxAjax.postSync(url,"codeAppForm","noJsonPreFlg").xmlDoc.responseText; 
		if(rest=="success")	{
			$("cUMTxt").innerHTML="发送成功";
			$("codeAppForm").reset();
		}else if(rest=="appShortTime"){
			$("cUMTxt").innerHTML="不执行发送，一小时内，只能发送一次";
		}else{
			$("cUMTxt").innerHTML="发送失败,请重试";
		}
	}
	function sendSubCheck(){
		if(isWhitespace($("companyName").value)){
			$("cUMTxt").innerHTML="公司名称不能为空";
			$("companyName").focus();
			return false;
		}else if(isWhitespace($("comWebSite").value)){
			$("cUMTxt").innerHTML="公司网址不能为空";
			$("comWebSite").focus();
			return false;
		}else if(isWhitespace($("comSize").value)){
			$("cUMTxt").innerHTML="公司规模不能为空";
			$("comSize").focus();
			return false;
		}else if(isWhitespace($("comAddress").value)){
			$("cUMTxt").innerHTML="公司地址不能为空";
			$("comAddress").focus();
			return false;
		}else if(isWhitespace($("telephone").value)){
			$("cUMTxt").innerHTML="联系电话不能为空";
			$("telephone").focus();
			return false;
		}else if(isWhitespace($("mailAddress").value)){
			$("cUMTxt").innerHTML="联系邮箱不能为空";
			$("mailAddress").focus();
			return false;
		}else if(!isEmailAddress($("mailAddress").value)){
			$("cUMTxt").innerHTML = "联系箱格式不正确";
			$("mailAddress").focus();
			return false;		
		}
		return true;
	}
	function isEmailAddress(value, allowEmpty){
	  var strText = new String(value);
	  if (allowEmpty == true && value.length == 0) {
	    return true;
	  }
	  var strPattern = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
	  var strPatt = /[^a-zA-Z0-9-@\._]/;
	  if (strText.match(strPatt)) {
	     return false;
	  }
	  aryResult = strText.match(strPattern);
	  if(aryResult == null) {
	  return false;
	  } else {
	  return true;
	 }
	}
	</script>
	</BODY>

</HTML>

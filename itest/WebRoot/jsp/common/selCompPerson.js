	var callBackCustom;
	var initGroypId="-1";
	var peopleId ;
	var peopleName ;
	var comUserSelW_ch;
	var sepaStr= " ";
	var seledGrid,selGrid,limitCount=0;
	var initPassValId = "";
	String.prototype.replaceAll  = function(oldText,newText){   
	    return this.replace(new RegExp(oldText,'g'),newText);   
	 };
	function popselWin(passValId,passNameId,idSepaStr,model,limitSel,callBack){
		if(typeof callBack == "undefined")
			callBackCustom = "";
		else
			callBackCustom = callBack;
		peopleId = passValId;
		peopleName = passNameId;
		if(typeof idSepaStr!="undefined"){
			sepaStr = idSepaStr;
		}
		if(typeof limitSel!="undefined")
			limitCount = limitSel;
		if(typeof comUserSelW_ch !="undefined"&&comUserSelW_ch.isHidden()){
			var seledPeopeNames = $(passNameId).value;
			seledPeopeNames = seledPeopeNames.replaceAll("，",",");
			var seledPeopeIds = $(passValId).value;
			seledGrid.clearAll();
			if(initPassValId != passValId){
				selGrid.clearAll();
  				selGrid.parse(eval("(" + initData +")"), "json");
  			}
  			initPassValId = passValId;
			if(seledPeopeIds!=""&&seledPeopeNames!=""){
				peopleIdArr = seledPeopeIds.split(sepaStr);
				peopleNameArr = seledPeopeNames.split(",");
				for(var i=0; i<peopleIdArr.length; i++){
					if(peopleNameArr[i]!=""){
						seledGrid.addRow2(peopleIdArr[i],"0,,"+peopleNameArr[i],i);
						selGrid.deleteRow(peopleIdArr[i]);
					}
				}			
			}
			if(typeof(model) !="undefined"){
				comUserSelW_ch.setModal(model);
			}else{
				comUserSelW_ch.setModal(true);
			}
			comUserSelW_ch.show();
			comUserSelW_ch.bringToTop();
			return;
		}
		if(typeof(importWinJs) != "undefined")
			importWinJs();
		if(typeof(model) !="undefined"){
			comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", model, 400, 357);
		}else{
			comUserSelW_ch = initW_ch(comUserSelW_ch, "selPeopleDiv", true, 400, 357);
		}
		if(typeof(model) =="undefined")
			comUserSelW_ch.button("close").attachEvent("onClick", function(){
				comUserSelW_ch.setModal(false);
				comUserSelW_ch.hide();
				recoverUser();
			});
		else
			comUserSelW_ch.button("close").attachEvent("onClick", function(){
				recoverUser();
				comUserSelW_ch.hide();
			});
		comUserSelW_ch.setText("选择人员");
		initSelGrid();//初始化grid
		initGroup("sel_groupIds");//初始化组下拦框
		initSleEdPeople(passValId,passNameId);//初始化grid数据
		comUserSelW_ch.bringToTop();
	}
	var initData="";
	function recoverUser(){
	    if(initData != ""&&initData!="failed"){
	    	selGrid.clearAll();
  			selGrid.parse(eval("(" + initData +")"), "json");
  		}	
	}
	function initSleEdPeople(passValId,passNameId){
		var seledPeopeIds = $(passValId).value;
		var seledPeopeNames = $(passNameId).value;
		seledPeopeNames = seledPeopeNames.replaceAll("，",",");
		peopleIdArr = seledPeopeIds.split(sepaStr);
		peopleNameArr = seledPeopeNames.split(",");
		seledGrid.clearAll();
		if(seledPeopeIds==""||seledPeopeNames==""){
			return;
		}
		for(var i=0; i<peopleIdArr.length; i++){
			if(peopleNameArr[i]!=""){
				seledGrid.addRow2(peopleIdArr[i],"0,,"+peopleNameArr[i],i);
				selGrid.deleteRow(peopleIdArr[i]);
			}
		}		
	}
	function initGroup(id){
		var groupUrl = conextPath+"/userManager/userManagerAction!groupSel.action";
		var groupSel  = dhtmlxAjax.postSync(groupUrl, "").xmlDoc.responseText;
		$(id).options.length = 1;
		if(groupSel != ""){
			var options = groupSel.split("^");
			for(var i = 0; i < options.length; i++){
				if(options[i].split(";")[0] != "")
					var selvalue = options[i].split(";")[0] ;
					var selable = options[i].split(";")[1];
					$(id).options.add(new Option(selable,selvalue));
			}
			if(initGroypId!="-1"){
				$("sel_groupIds").value=initGroypId;
			}
		}
	}

	function loadSelUser(isDefaultLoad){
		initGroypId = $3("sel_groupIds").value;
		var url = conextPath+"/userManager/userManagerAction!selectUser.action?dto.isAjax=true";
		if(typeof isDefaultLoad != "undefined"){
			url = conextPath+"/userManager/userManagerAction!loadDefaultSelUser.action?dto.isAjax=true";
		}
		var ajaxResut  = dhtmlxAjax.postSync(url, "selPeopleForm").xmlDoc.responseText;
		if(ajaxResut=="failed"){
  			if(typeof hintMsg!="undefined"){
  				hintMsg("加载数据发生错误");
  			}else{
  				showWarningMessage(true, "提醒", "加载数据发生错误");
  			}
  			return;
  		}
		selGrid.clearAll();
		ajaxResut = ajaxResut.split("$")[1];
	    if(ajaxResut != ""&&ajaxResut!="failed"){
   			ajaxResut = ajaxResut.replace(/[\r\n]/g, "");
   			initData = ajaxResut;
  			selGrid.parse(eval("(" + ajaxResut +")"), "json");
  			var allItems = seledGrid.getAllItemIds();
			var items = allItems.split(',');
			for(var i = 0; i < items.length; i++){
				selGrid.deleteRow(items[i]);
			}
  		}
	}
	
	function selPeople(){
		if(seledGrid.getRowsNum()==0){
			comUserSelW_ch.hide();
			comUserSelW_ch.setModal(false);
			$(peopleId).value = "";
			$(peopleName).value = "" ;
			return;
		}
		var allItems = seledGrid.getAllItemIds();
		var items = allItems.split(',');
		var ids = "";
		var names = "";
		for(var i=0; i<items.length;i++){
			ids = ids + sepaStr+items[i];
			names = names + ","+ seledGrid.cells(items[i], 2).getValue();
		}
		$(peopleId).value = ids.substring(1);
		$(peopleName).value = names.substring(1);
		comUserSelW_ch.setModal(false);
		comUserSelW_ch.hide();
		if(typeof(callBackCustom) != "undefined" && callBackCustom != ""){
			eval(callBackCustom);
		}
	}
	function initSelGrid(){
	    if(typeof selGrid == "undefined"){
		    selGrid = new dhtmlXGridObject('selGridbox');
			seledGrid = new dhtmlXGridObject('seledGridbox');
			selGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			seledGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
			selGrid.setHeader(",,备选人员--<span style='font-size:11px'>双击选择</span>");
			selGrid.setInitWidths("0,0,*");
	    	selGrid.setColAlign("left,left,left");
	    	selGrid.setColTypes("ch,ro,ro");
	   		selGrid.setColSorting("Str,int,str");
	   		selGrid.enableTooltips("false,false,true");
	    	selGrid.enableAutoHeight(true, 240);
	    	selGrid.setMultiselect(true);
	        selGrid.init();
	        selGrid.enableRowsHover(true, "red");
	    	selGrid.setSkin("light");
		    seledGrid.setHeader(",,己选人员--<span style='font-size:11px'>双击取消</span>");
			seledGrid.setInitWidths("0,0,*");
	    	seledGrid.setColAlign("left,left,left");
	    	seledGrid.setColTypes("ch,ro,ed");
	   		seledGrid.setColSorting("Str,int,str");
	   		seledGrid.enableTooltips("false,false,true");
	   		seledGrid.enableAutoHeight(true, 240);
	   		seledGrid.setMultiselect(true);
	        seledGrid.init();
	    	seledGrid.setSkin("light");	
	    	seledGrid.enableRowsHover(true, "red");
	    	seledGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
				selGrid.addRow2(rowId,"0,,"+seledGrid.cells(rowId,2).getValue(),0);
				seledGrid.deleteRow(rowId);
				seledGrid.setSizes();
			});
	    	selGrid.attachEvent("onRowDblClicked",function(rowId,cellInd,state){
				if(limitCount==1){
					$3(peopleId).value = rowId;
					$3(peopleName).value = selGrid.cells(rowId, 2).getValue();	
					comUserSelW_ch.setModal(false);
					comUserSelW_ch.hide();		
					seledGrid.addRow2(rowId,"0,,"+selGrid.cells(rowId,2).getValue(),0);
					selGrid.deleteRow(rowId);
					selGrid.setSizes();	
					return;
				}
				if(limitCount>0&&seledGrid.getRowsNum()>(limitCount-1)){
		  			if(typeof hintMsg!="undefined"){
		  				hintMsg("最多选择"+limitCount +"个用户");
		  			}else{
		  				showWarningMessage(true, "提醒", "最多选择"+limitCount +"个用户");
		  			}	
		  			return;		
				}
				seledGrid.addRow2(rowId,"0,,"+selGrid.cells(rowId,2).getValue(),0);
				selGrid.deleteRow(rowId);
				selGrid.setSizes();
			});
			loadSelUser('default');
	    }
	}
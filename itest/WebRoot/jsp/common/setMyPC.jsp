<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
	<script type="text/javascript">
		importJs(conextPath+"/jsp/common/setMyPC.js");
	</script>
	<div id="setMyPCDiv" class="gridbox_light" style="border:0px;display:none;">
		<div class="objbox" style="overflow:auto;width:100%;">
			<input type="hidden" id="progressId_T" name="progressId_T" />
			<form method="post" id="setMyPCF" name="setMyPCF" action="taskAction!setMyPC.action">
				<input type="hidden" id="progressId" name="taskDto.progressLog.id" />
				<input type="hidden" id="pcTaskId" name="taskDto.task.id" />
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td class="leftM_center">任务名称:</td><td id="setMyPC_name" class="dataM_left" colspan="3"><div id="setMyPC_name">&nbsp;</div></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">任务状态:</td><td id="setMyPC_TS" class="dataM_left" colspan="3"></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center">计划开始时间:</td><td id="setMyPC_PS" class="dataM_center" style="width:80px;width:85px !ie;"></td><td class="leftM_center">实际开始时间:</td><td id="setMyPC_FS" class="dataM_center"></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">计划结束时间:</td><td id="setMyPC_PE" class="dataM_center"><div>&nbsp;</div></td><td class="leftM_center">实际结束时间:</td><td id="setMyPC_FE" class="dataM_center"></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center">计划工期(日):</td><td id="setMyPC_PD" class="dataM_center"></td><td class="leftM_center">已用工期(日):</td><td id="setMyPC_FD" class="dataM_center"></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">任务优先级:</td><td id="setMyPC_TP" class="dataM_center"></td><td class="leftM_center">上次进度时间:</td><td id="perviousPCTime" class="dataM_center"></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center">上次进度(%):</td><td id="perviousPC" class="dataM_center"></td><td class="leftM_center" width="100px">上次进度结果:</td><td id="perviousPCResult" class="dataM_center"></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center" style="color:red;">任务进度(%):</td>
						<td class="dataM_center">
							<input type="text" id="percentCompleted" name="taskDto.progressLog.percentCompleted" class="text_c" style="width:30;" onkeypress="javascript:return numChk(event);" maxLength="3" />
						</td>
						<td class="leftM_center" style="color:red;width:90px;">填写进度时间:</td>
						<td class="dataM_center">
							<input type="text" id="pcDate" name="taskDto.progressLog.pcDate" class="text_c" style="width:80;" readonly="readonly" onclick="initDhtmlxCalendar();showCalendar(this, 'chosePCDate()');" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center" style="color:red;">所用工时(h):</td>
						<td style="padding-left:30px;border-right:0px;">
							<input type="text" id="wh" name="taskDto.progressLog.wh" class="text_c" style="width:30;" onkeypress="javascript:return numChk(event);" maxLength="3" />
						</td>
						<td colspan="2" style="border-left:0px;">
							<img id="smartMoreImg" src="<%=request.getContextPath()%>/jsp/common/images/smartMore.gif" onclick="searchCurrWh('', $3('pcDate').value, this);" title="查看当日所填工时信息" style="cursor:pointer;" />&nbsp;&nbsp;
							<img id="pcHistoryImg" src="<%=request.getContextPath()%>/jsp/common/images/pcHistory.gif" onclick="searchTaskPCHistory($3('setMyPC_name').innerHTML, $3('pcTaskId').value, this);" title="查看任务的进度历史" style="cursor:pointer;" />&nbsp;&nbsp;
							<img id="customTaskAddImg" src="<%=request.getContextPath()%>/jsp/common/images/customTaskAdd.gif" onclick="addCustom($3('pcDate').value, this, $3('pcTaskId').value);" title="添加通用任务工时信息" style="cursor:pointer;" />&nbsp;&nbsp;
							<img id="addTaskWHImg" src="<%=request.getContextPath()%>/jsp/common/images/addTaskWH.gif" onclick="addParentWH($3('setMyPC_name').innerHTML, $3('pcTaskId').value, $3('pcDate').value)" title="填写任务工时信息" style="cursor:pointer;" />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">填写进度类型:</td>
						<td colspan="3" style="padding-left:5px;">
							<input type="radio" name="taskDto.progressLog.isCreatePC" id="isCreatePC1" checked="checked" value="1"/><label for="isCreatePC1">&nbsp;添加新进度</label>&nbsp;&nbsp;
							<input type="radio" name="taskDto.progressLog.isCreatePC" id="isCreatePC0" value="0" /><label for="isCreatePC0">&nbsp;修改进度</label>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center" style="width:90px;">填写进度描述:</td>
						<td colspan="3" class="dataM_left"><textarea id="remark_PC" name="taskDto.progressLog.remark" style="width:260px;height:50px !ie;" class="textarea" rows="2" cols=""></textarea></td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4"><div id="setMyPCText" class="waring">&nbsp;</div></td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="4" align="right" style="padding-right:8px;"><img id="setMyPCSaveImg" src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="setMYPC();" title="确定" style="cursor:pointer;" />&nbsp;&nbsp;<img id="setMyPCResetImg" src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('setMyPCF', 'percentCompleted');$3('setMyPCResetImg').style.display = '';$3('setMyPCSaveImg').style.display = '';" title="重置" style="cursor:pointer;" /></td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="4" >&nbsp;</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div id="setParentTaskWHDiv" class="gridbox_light" style="border:0px;display:none;">
		<div class="objbox" style="overflow:auto;width:100%;">
			<form method="post" id="addParentTaskWHF" name="addParentTaskWHF" action="">
				<input type="hidden" id="taskUserLongTaskId" name="taskDto.taskUserLog.taskId" />
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td class="leftM_center" style="width:80px;">任务名称:</td>
						<td><div id="taskNameWHDiv"></div></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center" style="color:red;">填写时间:</td>
						<td><input type="text" id="progressDateStringParent" name="taskDto.taskUserLog.progressDateString" class="text_c" style="width:80;" readonly="readonly" onclick="initDhtmlxCalendar();showCalendar(this, 'choseParentTaskDate(0)');"/></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center" style="color:red;">所用工时(h):</td>
						<td><input type="text" id="custom_whParent" name="taskDto.taskUserLog.wh" class="text_c" style="width:30;" onkeypress="javascript:return numChk(event);" maxLength="3" /></td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="2" class="leftM_center"><div id="custom_cTMParent" class="waring"></div></td>
					</tr>
					<tr class="ev_mypm">
						<td align="right" colspan="2">
							<img id="addButtomParent" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="addParentTaskWH();" title="保存" />&nbsp;&nbsp;
							<img id="saveTaskParentResetImg" style="cursor:pointer;padding-right:10px;" src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('addParentTaskWHF', 'custom_whParent');buttomParentDis(true);$3('custom_cTMParent').innerHTML = '';" title="重置" />
						</td>
					</tr>
					<tr class="odd_mypm">
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div id="addCustomTaskDiv" class="gridbox_light" style="border:0px;display:none;">
		<div class="objbox" style="overflow:auto;width:100%;">
			<form method="post" id="addCustomTaskLogF" name="addCustomTaskLogF" action="">
				<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
					<tr class="ev_mypm">
						<td class="leftM_center" style="color:red;">通用任务名称:</td>
						<td><select id="customTaskId" name="taskDto.customTaskLog.customTaskId" class="select_c" style="width: 120px;"></select></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">参与项目名称:</td>
						<td><select id="customProjectId" name="taskDto.customTaskLog.customProjectId" class="select_c" style="width: 120px;"></select></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center" style="color:red;">填写时间:</td>
						<td><input type="text" id="progressDateString" name="taskDto.customTaskLog.progressDateString" class="text_c" style="width:80;" readonly="readonly" onclick="initDhtmlxCalendar();showCalendar(this, 'choseParentTaskDate(1)');"/></td>
					</tr>
					<tr class="ev_mypm">
						<td class="leftM_center" style="color:red;">所用工时(h):</td>
						<td><input type="text" id="custom_wh" name="taskDto.customTaskLog.wh" class="text_c" style="width:30;" onkeypress="javascript:return numChk(event);" maxLength="3" /></td>
					</tr>
					<tr class="odd_mypm">
						<td class="leftM_center">通用任务描述:</td>
						<td class="dataM_left">
							<textarea style="width: 236px;height:50px !ie;" class="textarea" rows="2" cols="" class="textarea" id="custom_remark" name="taskDto.customTaskLog.remark"></textarea>
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="2" class="leftM_center"><div id="custom_cTM" class="waring"></div></td>
					</tr>
					<tr class="odd_mypm">
						<td align="right" colspan="2">
							<img id="addButtom" style="cursor:pointer;" src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="addCustomTaskLog();" title="保存" />&nbsp;&nbsp;
							<img id="saveTaskResetImg" style="cursor:pointer;padding-right:10px;" src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="formReset('addCustomTaskLogF', 'custom_wh');buttomDis(true);$3('custom_cTM').innerHTML = '';" title="重置" />
						</td>
					</tr>
					<tr class="ev_mypm">
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<div id="setMyPC100MessageDiv" class="gridbox_light" style="border:0px;display:none;z-index:99;">
		<div class="objbox" style="overflow:auto;width:100%;">
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
				<tr class="odd_mypm"><td>&nbsp;</td></tr>
				<tr class="odd_mypm"><td class="mypm_center"><img src="<%=request.getContextPath()%>/jsp/common/images/question.gif" /></td></tr>
				<tr class="odd_mypm"><td class="mypm_center">此任务已经完成 您确认要修改此任务的进度吗</td></tr>
				<tr class="odd_mypm"><td colspan="3" align="right" style="padding-right:8px;"><img src="<%=request.getContextPath()%>/jsp/common/images/save.gif" onclick="excuteSetPC();" title="确定" style="cursor:pointer;" />&nbsp;&nbsp;<img src="<%=request.getContextPath()%>/jsp/common/images/reset.gif" onclick="setMyPC100W_ch.hide();setMyPC100W_ch.setModal(false);" title="取消" style="cursor:pointer;" /></td></tr>
				<tr class="odd_mypm"><td>&nbsp;</td></tr>
			</table>
		</div>
	</div>
	<div id="pcHistoryWinDiv" style="border:0px;display:none;">
		<div id="pcHistoryText" style="width:525;font-family: Tahoma;font-weight: bold;color:#055A78;font-size:12px;text-align:left;vertical-align:middle;padding-left:5px;">&nbsp;</div>
		<div id="pcHistoryDiv" style="width:525;"></div>
	</div>
	<ww:include value="/jsp/common/setMyWh.jsp"></ww:include>
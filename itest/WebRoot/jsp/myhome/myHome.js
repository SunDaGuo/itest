	var taskTypeArray = new Array("普通任务", "集成测试", "系统测试");
	var taskStatusArray = new Array("未开始","滞后","正常进行","提前开始","提前结束","正常结束","延期结束","人为终止","延期","");
	var taskPrecddenceArray = new Array();taskPrecddenceArray[-2]="最低";taskPrecddenceArray[-1]="低";taskPrecddenceArray[0]="一般";taskPrecddenceArray[1]="高";taskPrecddenceArray[2]="最高";
	var pageSize=10,autoHeight=120;
	function ininHomePage(){
		$("msgDivTitle").style.width = gridWidth - 10;
		$("bugTitle").style.width = gridWidth - 10;
		$("tasksTitle").style.width = gridWidth - 10;
		if(_isIE){
			$("mypmHomeMsgDiv").style.width = gridWidth - 10;
			$("bugDiv").style.width = gridWidth - 10;
			$("taskDiv").style.width = gridWidth - 10;
		}else{
			$("mypmHomeMsgDiv").style.width = gridWidth - 12;
			$("bugDiv").style.width = gridWidth - 12;
			$("taskDiv").style.width = gridWidth - 12;
		}
	}
	ininHomePage();
	
	if(myHome.length==1){
		pageSize=20;
		autoHeight=650;
	}else if(myHome.length==2){
		pageSize=15;
		autoHeight=320;
	}
	document.getElementById("logoTable").style.width = clientWidth - 10;
	var bugGrid,msgGrid,msgDetalW_ch,taskGrid;
	function initPage(){
		if(myHome.indexOf("1")>=0)
			loagMsg();
		else
			document.getElementById("msgTr").style.display = "none";
		if(myHome.indexOf("2")>=0)
			loadBug();
		else
			document.getElementById("bugTr").style.display = "none";
		if(myHome.indexOf("3")>=0)
			loadTask();
		else
			document.getElementById("taskTr").style.display = "none";
		return;
	}
	initPage();
	function loagMsg(){
		$("mypmHomeMsgDiv").style.display="";
		$("msgDivTitle").style.display="";
		msgGrid = new dhtmlXGridObject('mypmHomeMsgDiv');
		msgGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	    msgGrid.setHeader("&nbsp;,序号,消息标题,类型,发布人,发布日期,附件");
	    msgGrid.setInitWidths("0,40,*,80,120,80,60");
	    msgGrid.setColAlign("center,center,left,left,left,center,center");
	    msgGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	    msgGrid.setColSorting("int,str,str,str,str,str,str");
		msgGrid.enableAutoHeight(true, autoHeight);
	    msgGrid.init();
	    msgGrid.enableRowsHover(true, "red");
	    msgGrid.enableTooltips("false,true,true,true,true,true,true");
	    msgGrid.setSkin("light");
	    msgGrid.attachEvent("OnCheck",doMsgOnCheck);
	    msgGrid.attachEvent("onRowSelect",doMsgOnSelect);
	    var url =conextPath+"/commonAction!loadMyHomeMsg.action?dto.pageSize="+pageSize;
	    var result = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
	    if(result!="failed"){
	    	msgColTypeReset();
	    	initGrid(msgGrid,result);
	    	msgLoadLink();
	    }else{
	    	$("hintMsgTxt").innerHTML="加载我的消息发生错误";
	    }	
	}
	function loadBug(){
		$("bugDiv").style.display="";
		$("bugTitle").style.display="";
		bugGrid = new dhtmlXGridObject('bugDiv');
		bugGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
	    bugGrid.setHeader("&nbsp;,序号,Bug描述,测试人员,发现版本,状态,报告日期,附件,&nbsp;");
	    bugGrid.setInitWidths("0,40,*,80,80,60,80,60,0");
	    bugGrid.setColAlign("center,center,left,left,left,center,center,center,left");
	    bugGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    bugGrid.setColSorting("int,str,str,str,str,str,str,str,str");
		bugGrid.enableAutoHeight(true, autoHeight);
	    bugGrid.init();
	    bugGrid.enableRowsHover(true, "red");
	    bugGrid.enableTooltips("false,true,true,true,true,true,true,true,false");
	    bugGrid.setSkin("light");
	    bugGrid.attachEvent("OnCheck",doBugOnCheck);
	    bugGrid.attachEvent("onRowSelect",doBugOnSelect);
	    var url =conextPath+"/commonAction!loadMyHomeBug.action?dto.pageSize="+pageSize;
	    var result = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
	    if(result!="failed"){
	    	bugColTypeReset();
	    	initGrid(bugGrid,result);
	    	bugLoadLink();
	    }else{
	    	$("hintMsgTxt").innerHTML="加载我的Bug发生错误";
	    } 
	}
	function loadTask(){
		$("taskDiv").style.display="";
		$("tasksTitle").style.display="";
		taskGrid = new dhtmlXGridObject('taskDiv');
		taskGrid.setImagePath(conextPath + "/dhtmlx/grid/codebase/imgs/");
	    taskGrid.setHeader("&nbsp;,序号,<div title='点击项目名称链接进入所属项目任务列表'>项目名称</div>,任务名称,&nbsp;&nbsp;任务类型,&nbsp;优先级,计划开始时间,实际开始时间,计划结束时间,<div title='点击进度链接直接填写进度'>&nbsp;&nbsp;进度</div>,&nbsp;任务状态,&nbsp;");
	    taskGrid.setInitWidths("0,40,150,*,90,70,95,95,95,60,80,0");
	    taskGrid.setColAlign("center,center,left,left,center,center,center,center,center,center,center,center");
	    taskGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	    taskGrid.setColSorting("int,int,str,str,str,str,str,str,str,str,str,str");
		taskGrid.enableAutoHeight(true, autoHeight);
	    taskGrid.init();
	    taskGrid.enableRowsHover(true, "red");
	    taskGrid.enableTooltips("false,false,false,false,true,true,true,true,true,true,true,false");
	    taskGrid.setSkin("light");
	   	var url =conextPath+"/task/taskAction!todayTasks.action?taskDto.myTask.todayFlag=1";
	    var result = dhtmlxAjax.postSync(url, "").xmlDoc.responseText;
	    if(result!="failed"){
	    	initTaskDatas(result);
	    	taskGrid.setColTypes("ro,ro,link,ro,ro,ro,ro,ro,ro,link,ro,ro");
	    }else{
	    	$("hintMsgTxt").innerHTML="加载今日任务发生错误";
	    }
	}
    function doMsgOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
    function doMsgOnSelect(rowId,index){
		msgGrid.cells(rowId, 0).setValue(true);
		msgGrid.setSelectedRow(rowId);
	}
	
	function msgColTypeReset(){
		msgGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro");
	}
	function msgSw2Link(){
		msgGrid.setColTypes("ro,ro,link,ro,ro,ro,link");
	}
	function bugColTypeReset(){
		bugGrid.setColTypes("ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function bugSw2Link(){
		bugGrid.setColTypes("ro,ro,link,ro,ro,ro,ro,link,ro");
	}			
	function msgLoadLink(){
		for(var i = 0; i<msgGrid.getRowsNum(); i++){
			msgGrid.cells2(i,2).cell.innerHTML="<a href='javascript:viewMsgDetl()' title='查看详情'>"+msgGetTttle2(i,2)+"</a>";
			if(msgGetTttle2(i,6)!=""&&msgGetTttle2(i,6).indexOf("src")<0){
				var attach = msgGetTttle2(i,6);
				attach = attach.substring(attach.indexOf("_")+1)
				msgGrid.cells2(i,6).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='"+attach+"' onclick=\"openAtta('"+msgGetTttle2(i,6)+"')\"/>";
			}		
		}
		msgSw2Link();
	} 
	function bugLoadLink(){
		for(var i = 0; i <bugGrid.getRowsNum(); i++){
			bugGrid.cells2(i,2).cell.innerHTML="<a href='javascript:viewBugDetl()' title='查看详情'>"+bugGetTttle2(i,2)+"</a>";
			if(bugGetTttle2(i,7)!=""&&bugGetTttle2(i,7).indexOf("src")<0){
				var attach = bugGetTttle2(i,7);
				attach = attach.substring(attach.indexOf("_")+1)
				bugGrid.cells2(i,7).cell.innerHTML="<img src='"+conextPath+"/images/button/attach.gif' alt='附件'  title='"+attach+"' onclick=\"openAtta('"+bugGetTttle2(i,7)+"')\"/>";
			}		
		}
		bugSw2Link();
	} 
	function msgGetTttle2(rowNum,colIn){
		return msgGrid.cells2(rowNum,colIn).getValue();
	}
	function bugGetTttle2(rowNum,colIn){
		return bugGrid.cells2(rowNum,colIn).getValue();
	}  
    function doBugOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		return true;
	}
    function doBugOnSelect(rowId,index){
		bugGrid.cells(rowId, 0).setValue(true);
		bugGrid.setSelectedRow(rowId);
	}	
	function initGrid(GridObj,gridData){
		var data = gridData;
	    if(data != ""){
	    	data = data.replace(/[\r\n]/g, "");
		    var datas = data.split("$");
		    if(datas[1] != ""){
		    	GridObj.clearAll();
		    	jsons = eval("(" + datas[1] +")");
		    	GridObj.parse(jsons, "json");
				for(var i = 0; i < GridObj.getRowsNum(); i++){
					GridObj.cells2(i,1).setValue((i + 1));
				}	
		    }
	    }	
	}
	var oEditor ; 
	function viewMsgDetl(){
		var url =conextPath+"/msgManager/commonMsgAction!viewDetal.action?dto.isAjax=true&dto.broMsg.logicId="+msgGrid.getSelectedId();
		url+="&dto.isView=1";
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			$("hintMsgTxt").innerHTML="加载我的消息发生错误";
			return;
		}else if(ajaxRest.indexOf("failed^")>=0){
			hintMsg(ajaxRest.split("^")[1]);
			return;			
		}
		$("currAttach").style.display="none";
		setMsgUpInfo(ajaxRest);
		msgDetalW_ch=initW_ch(msgDetalW_ch, "msgDetalDiv", true,660, 480);
		loadFCK();
		msgDetalW_ch.setText("消息明细");
		adjustTable('msgDetalTab');
		msgDetalW_ch.bringToTop();
		msgDetalW_ch.setModal(true);
		return;
	}
	function setMsgUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1]);
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);
				if(currInfo[0]=="content"){
					$("initContent").value=currInfo[1];
				}else if(currInfo[0]=="mailFlg"&&currInfo[1]=="true"){
					$("mailFlg").checked=true;
				}else if(currInfo[0]=="mailFlg"&&currInfo[1]!="true"){
					$("mailFlg").checked=false;
				}else if(currInfo[0]=="msgType"&&currInfo[1]=="1"){
					$("recpiUserIdTr").style.display="";
				}else if(currInfo[0]=="msgType"&&currInfo[1]!="1"){
					$("recpiUserIdTr").style.display="none";
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach").style.display="";
					$("currAttach").title="附件";
					//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
				}
			}
		}
	}
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			oEditor.SetData($("initContent").value) ;
			return;
		}
		importJs(conextPath+"/pmEditor/fckeditor.js");
    	var pmEditor = new FCKeditor('content') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 300;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('content') ;
		oEditor.SetData($("initContent").value) ;
		return;
	}
 	function adjustTable(tableId,startClass){
		var ctable=$(tableId);
		var count = 1;
		for(var i=0;i<ctable.rows.length;i++){
		   if(ctable.rows[i].style.display==""){
			   if((count%2)==0){
				   	if(typeof(startClass)!="undefined"){
			   	   		ctable.rows[i].className="ev_mypm";
			   	   	}else{
				   		ctable.rows[i].className="odd_mypm";
				   	}
			   }else{
				   	if(typeof(startClass)!="undefined"){
			   	   		ctable.rows[i].className="odd_mypm";
			   	   	}else{
				   		ctable.rows[i].className="ev_mypm";
				   	}
			   }
			   count++;
			  }
		}
	}
    function initTaskDatas(jsonDatas){
	    if(jsonDatas != ""){
			taskGrid.parse(eval("(" + jsonDatas +")"), "json");
			initTaskRowDatas();
		}else{
		}
	}
	
	function initTaskRowDatas(){
		var allItems = taskGrid.getAllItemIds();
		if(allItems == "")return;
		var taskType, status, precddence,projectName,ids,projectId, taskId, childs;
		var items = allItems.split(',');
		for(var i = 0; i < items.length; i++){
			taskType = taskGrid.cells(items[i],4).getValue();
			status = taskGrid.cells(items[i],10).getValue();
			precddence = taskGrid.cells(items[i],5).getValue();
			projectName = taskGrid.cells(items[i],2).getValue();
			ids = taskGrid.cells(items[i],11).getValue().split("-");
			projectId = ids[2];taskId=ids[1];childs=ids[0];
			taskGrid.cells(items[i],1).setValue(i + 1);
			taskGrid.cells(items[i],4).setValue(taskTypeArray[taskType]);
			taskGrid.cells(items[i],5).setValue(taskPrecddenceArray[precddence]);
			taskGrid.cells(items[i],10).setValue(taskStatusArray[status]);
			taskGrid.cells(items[i],2).setValue("<a href=#; title='进入所属项目任务列表' target=_self onclick=javascript:linkTaskList('"+projectId+"'); style=TEXT-DECORATION:none;>" + projectName + "</a>");
			//if(childs == "0")
				taskGrid.cells(items[i],9).setValue("<a href=javascript:void(0); title='填写进度' target=_self onclick=(_isIE?event:arguments[0]).cancelBubble=true;setTaskPC('"+taskId+"','"+projectId+"','"+childs+"'); style=TEXT-DECORATION:none>" + taskGrid.cells(items[i],9).getValue() + "</a>");
			if(ids[3] != "null" && ids[3] != ""){
				//taskGrid.setCellTextStyle(items[i],8,colorStr);
				taskGrid.setCellColor(items[i],1,ids[3]);
				taskGrid.setCellTextStyle(items[i],9,"font-family: Arial;font-weight: bold;font-size:14px;");
			}
			if(items[i].indexOf("mypm") != -1){
				//var colorStr = "font-family: Arial;font-weight: bold;font-size:14px;color:" + ids[3];
				//taskGrid.setRowTextStyle(items[i], colorStr);
				taskGrid.setCellColor(items[i],1,ids[3]);
				taskGrid.setCellTextStyle(items[i],9,"font-family: Arial;font-weight: bold;font-size:14px;");
			}
		}
	}
	
	function setTaskPC(taskId,projectId,childs){
		var name = "";
		try{name = taskGrid.cells(taskId, 3).getValue();}catch(e){name = taskGrid.cells(taskId + "mypm", 3).getValue();}
		initPCWindow(taskId, projectId, name);
	}
	
	function linkTaskList(proId){
		if(!canViewTodayTask){
			hintMsg("您没有浏今日任务权限");
			return;
		}
		var rest = dhtmlxAjax.postSync(conextPath+"/commonAction!goTaskHome.action?dto.projectId="+proId,"").xmlDoc.responseText;
		if(rest=="success")
			window.parent.location=conextPath+"/jsp/common/main.jsp";
		else
		  hintMsg("初始化任务列表发生错误");
	}
	var detlW_ch;
	function viewBugDetl(){
		detlW_ch = initW_ch(detlW_ch, "", true, 810, 520,'detlW_ch');
		var url= conextPath +"/bugManager/bugManagerAction!viewBugDetal.action";
		var bugId = bugGrid.getSelectedId();
		url+="?dto.bug.bugId="+bugId+"&dto.taskId="+bugGrid.cells(bugId,8).getValue();
		detlW_ch.attachURL(url);	
	    detlW_ch.show();
	    detlW_ch.bringToTop();
	    detlW_ch.setModal(true);
	    detlW_ch.setText("软件问题报告明细");
	    return;
	}
	detlW_ch = initW_ch(detlW_ch, "", true, 800, 520,'detlW_ch');
	detlW_ch.setModal(false);
	detlW_ch.hide();
	function goProLst(){
		window.parent.location=conextPath+"/jsp/common/main.jsp";
	}
	function openAtta(fileName){
		if(typeof fileName!="undefined"){
			$("downloadFileName").value=fileName;
		}else if($("attachUrl").value!=""){
			$("downloadFileName").value=$("attachUrl").value;
		}else{
			return;
		}
		$("downForm").action=conextPath+"/fileUpload?cmd=download";
		$("downForm").submit();
	}
<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="java.util.Date"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
 
<HTML>
	<HEAD>
		<TITLE>消息列表</TITLE>
		<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid_skins.css">
		<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/skins/dhtmlxtoolbar_dhx_blue.css">
		<link rel='STYLESHEET' type='text/css' href='<%=request.getContextPath()%>/css/page2.css'>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgridcell.js"></script>
		<script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/toolbar/codebase/dhtmlxtoolbar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/commonFunction_top.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/globalVariable.js"></script>
	    <script type="text/javascript"src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/excells/dhtmlxgrid_excell_link.js"></script>
	</HEAD>
	<BODY bgcolor="#ffffff" style="overflow-x:hidden;">
	 
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" style="width:100%">
					<div id="toolbarObj" width="100%"></div>
				</td>
			</tr>
			<tr>
				<td valign="top">
					<div id="gridbox" height="1000" width="100%"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="listStr" name="listStr"
			value="${dto.listStr}" />
	<script type="text/javascript">
	ininPage("toolbarObj", "gridbox", 580);
	var myName = "${session.currentUser.userInfo.name}";
	pmBar = new dhtmlXToolbarObject("toolbarObj");
	pmBar.setIconsPath(conextPath+"/dhtmlx/toolbar/images/");
	pmBar.addButton("find",1 , "", "search.gif");
	pmBar.setItemToolTip("find", "查询");
	
	pmBar.addButton("reFreshP",2 , "", "page_refresh.gif");
	pmBar.setItemToolTip("reFreshP", "刷新页面");
	pmBar.addButton("back",3 , "", "back.gif");
	pmBar.setItemToolTip("back", "返回");
	pmBar.addButton("first",4 , "", "first.gif", "first.gif");
	pmBar.setItemToolTip("first", "第一页");
	pmBar.addButton("pervious",5, "", "pervious.gif", "pervious.gif");
	pmBar.setItemToolTip("pervious", "上一页");
	pmBar.addSlider("slider",6, 80, 1, 30, 1, "", "", "%v");
	pmBar.setItemToolTip("slider", "滚动条翻页");
	pmBar.addButton("next",7, "", "next.gif", "next.gif");
	pmBar.setItemToolTip("next", "下一页");
	pmBar.addButton("last",8, "", "last.gif", "last.gif");
	pmBar.setItemToolTip("last", "末页");
	pmBar.addInput("page",9, "", 25);
	pmBar.addText("pageMessage",10, "");
	pmBar.addText("pageSizeText",11, "每页");
	var opts = Array(Array('id1', 'obj', '10'), Array('id2', 'obj', '15'), Array('id3', 'obj', '20'), Array('id4', 'obj', '25'), Array('id5', 'obj', '30'));
	pmBar.addButtonSelect("pageP",12, "page", opts);
	pmBar.setItemToolTip("pageP", "每页记录数");
	pmBar.addText("pageSizeTextEnd",13, "条");
	pmBar.addSeparator("sep_swTask2",14); 
	pmBar.addButton("currPosition",15, "当前位置: 接收消息", "hand.gif", "hand.gif");
	pmBar.attachEvent("onClick", function(id) {
		if(id =="reFreshP"){
	   pageAction(pageNo, pageSize);
		}
	});
	
	pmBar.attachEvent("onClick", function(id) {
		if(id =="back"){
	   var reUrl= document.referrer;
	   if(reUrl!=""&&(reUrl.indexOf('main.jsp')>0||reUrl.indexOf('myHome.jsp')>0)){
	   	history.back();
	   	return;
	   }
	   parent.mypmMain.location=reUrl;
		}
	});
	pmGrid = new dhtmlXGridObject('gridbox');
	pmGrid.setImagePath(conextPath+"/dhtmlx/grid/codebase/imgs/");
    pmGrid.setHeader("&nbsp;,序号,标题,类型,状态,发布人,发布日期,生效日期,过期日期,附件");
    pmGrid.setInitWidths("40,40,*,50,50,100,80,80,80,60");
    pmGrid.setColAlign("center,center,left,left,left,left,center,center,center,center");
    pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro");
    pmGrid.setColSorting("int,str,str,str,str,str,str,str,str,str");
	pmGrid.attachEvent("onCheckbox",doOnCheck);
	pmGrid.enableAutoHeight(true, 700);
    pmGrid.init();
    pmGrid.enableRowsHover(true, "red");
    pmGrid.enableTooltips("false,true,true,true,true,true,true,true,true,true,true");
    pmGrid.setSkin("light");
    initGrid(pmGrid,"listStr");	
    pmGrid.attachEvent("onRowSelect",doOnSelect);  
    function doOnCheck(rowId,cellInd,state){
		this.setSelectedRow(rowId);
		try{
			if(pmGrid.cells(rowId,4).getValue()=="己生效"){
			  pmBar.disableItem("update");
			}else{
				pmBar.enableItem("update");
			}
		}catch(err){
		}
		return true;
	}
    function doOnSelect(rowId,index){
		pmGrid.cells(rowId, 0).setValue(true);
		try{
			if(pmGrid.cells(rowId,4).getValue()=="己生效"){
			  pmBar.disableItem("update");
			}else{
				pmBar.enableItem("update");
			}
		}catch(err){
		}
	}
	function colTypeReset(){
		pmGrid.setColTypes("ra,ro,ro,ro,ro,ro,ro,ro,ro,ro");
	}
	function sw2Link(){
		pmGrid.setColTypes("ra,ro,link,ro,ro,ro,ro,ro,ro,ro");
	}	
	loadLink();	
	function loadLink(){	
		var fileName="";
		for(var i = 0; i <pmGrid.getRowsNum(); i++){
			pmGrid.cells2(i,2).cell.innerHTML="<a href='javascript:viewDetl()' title='查看详情'>"+getTttle2(i,2)+"</a>";
			if(getTttle2(i,9)!=""){
				pmGrid.cells2(i,9).setValue("<img src="+conextPath+"/images/button/attach.gif' alt='附件'  title='ddddddddd'onclick=\"openAtta('"+getTttle2(i,9)+"')\"/>");
			}
		}
		sw2Link();
	} 
	function getTttle2(rowNum,colIn){
		return pmGrid.cells2(rowNum,colIn).getValue();
	} 
	</script>
		<div id="createDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		   <div class="objbox" style="overflow:auto;width:100%;">
			 <input type="hidden" id="initContent" name="initContent"/>
			 <input type="hidden" id="startDateInit" name="startDateInit"/>
			 <ww:hidden id="sendName" name="sendName"></ww:hidden>
			 <ww:hidden id="attachUrl" name="dto.broMsg.attachUrl"></ww:hidden>
			<ww:form theme="simple" method="post" id="createForm" name="createForm" namespace="" action="">
			<ww:hidden id="logicId" name="dto.broMsg.logicId"></ww:hidden>
			<ww:hidden id="state" name="dto.broMsg.state"></ww:hidden>
			<ww:hidden id="senderId" name="dto.broMsg.senderId"></ww:hidden>
			<ww:hidden id="sendDate" name="dto.broMsg.sendDate"></ww:hidden>
			<ww:hidden id="recpiUserId" name="dto.broMsg.recpiUserId"></ww:hidden>
			<ww:hidden id="attachUrl" name="dto.broMsg.attachUrl"></ww:hidden>
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%" id="baseInfoTab">
			<tr>
			  <td width="600"colspan="4">&nbsp;
			  </td>
			</tr>
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 标题:
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<ww:textfield id="msgTitle" name="dto.broMsg.title" readonly="true" cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" align="right" nowrap>
			  	 生效日期: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left">
					<ww:textfield id="startDate" readonly="true" name="dto.broMsg.startDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  	 <td class="rightM_center" width="80" align="right">
			  	 失效日期: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left">
					<ww:textfield id="overdueDate" readonly="true" name="dto.broMsg.overdueDate" cssStyle="padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 类型: 
			  	 </td>
			    <td class="dataM_left" width="100" align="left">
					<ww:select id="msgType" name="dto.broMsg.msgType"
						list="#{-1:'',0:'广播',1:'限定接收人'}" headerValue="-1" cssStyle="width:100;" cssClass="text_c">
					</ww:select>
			    </td>
			  	 <td class="rightM_center" width="80" align="right">
			  	 同时Mail告知:
			  	 </td>
			    <td  width="100" align="left" class="dataM_left">
			        <ww:checkbox name="dto.broMsg.mailFlg" id="mailFlg" disabled="true" value="FLASE" fieldValue="1" cssClass="text_c"/>
			    </td>
			  </tr>	
			  <tr id="recpiUserIdTr" style="display:none">
			  	 <td class="rightM_center" width="80" align="right">
			  	 接收人: 
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<ww:textfield id="recpiUserName" name="recpiUserName" readonly="true"   cssStyle="width:560;padding:2 0 0 4;" cssClass="text_c"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr>
			  	 <td class="rightM_center" width="80" align="right">
			  	 内容: 
			  	 </td>
			    <td class="dataM_left" width="560" colspan="3"align="left">
					<textarea name="dto.broMsg.content" id="content" cols="50"
							rows="30" style="width:560;hight:200;padding:2 0 0 4;"  Class="text_c">
					</textarea>	
			    </td>
			  </tr>
			<tr>
			<td class="rightM_center" width="80" align="right">附件:</td>
			  <td width="560"colspan="3" align="left"><img src="<%=request.getContextPath()%>/images/button/attach.gif"  style="display:none"id="currAttach" alt="当前附件" title="打开附件" onclick="openAtta()" />
			  </td>
			</tr>
			<tr>
				<td class="tdtxt" align="center" width="640" colspan="4">
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript:cW_ch.setModal(false);cW_ch.hide();"
						style="margin-left: 6px"><span> 返回</span> </a>
				<td>
			</tr>	
			<tr>
			  <td width="600"colspan="4">&nbsp;
			  </td>
			</tr>			
			</table>
			</ww:form>
			<form action="downloadfile" method="post" id="downForm" name="downForm">
				<input type="hidden" id="downloadFileName" name="downloadFileName" value=""/>
		   </form>
		   </div>
		</div>	
		<div id="findDiv" class="cycleTask gridbox_light" style="border:0px;display:none;">
		<div class="objbox" style="overflow:auto;width:100%;">
			<ww:form theme="simple" method="post" id="findForm" name="findForm"
				namespace="" action="">
			<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			  <tr class="ev_mypm">
			    <td colspan="4" width="500" style="border-right:0">&nbsp;
			    </td>
			  </tr>
			  <tr class="odd_mypm">
			  	 <td class="rightM_center" width="125" align="right" style="border-right:0">
			  	 标题/内容:
			  	 </td>
			    <td class="dataM_left" width="375" colspan="3"align="left" style="border-right:0">
					<ww:textfield id="msgTitleF" name="dto.broMsg.title" cssClass="text_c" cssStyle="width:375;padding:2 0 0 4;"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="ev_mypm">
			  <ww:hidden id="senderIdF" name="dto.broMsg.senderId"></ww:hidden>
			  	 <td class="rightM_center" width="125" align="right"style="border-right:0" >
			  	 发布人:
			  	 </td>
			    <td class="dataM_left" width="375" colspan="3"align="left" style="border-right:0">
					<ww:textfield id="senderNameF" name="senderNameF" readonly="true"  cssClass="text_c" onfocus="popselWin('senderIdF','senderNameF');" cssStyle="width:375;padding:2 0 0 4;"> </ww:textfield>			    
			    </td>
			  </tr>	
			  <tr class="odd_mypm">
			    <td width="500" colspan="5" hight="0">&nbsp;
			    </td>
			  </tr>
			<tr class="ev_mypm">
				<td class="tdtxt" align="center" width="500" colspan="5" style="border-right:0">
					<a class="bluebtn" href="javascript:void(0);"
						onclick="javascript:fW_ch.setModal(false);fW_ch.hide();"
						style="margin-left: 6px"><span> 返回</span> </a>
					<a class="bluebtn" href="javascript:void(0);" id="queryBtn"onclick="findExe()"
						style="margin-left: 6px;"><span> 确定</span> </a>
				<td>
			</tr>	
		  <tr class="odd_mypm">
		    <td colspan="5">&nbsp;</td>
		  </tr>		 			
			</table>	
			</ww:form>
		 </div>
		</div>
		<ww:include value="/jsp/common/selCompPerson.jsp"></ww:include>
		<ww:include value="/jsp/common/dialog.jsp"></ww:include>
	</BODY>
	<script type="text/javascript">

	</script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/jsp/broadcastMsg/receivedBroMsg.js"></script>
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/dhtmlxwindows.css">
	<link rel="STYLESHEET" type="text/css"href="<%=request.getContextPath()%>/dhtmlx/windows/codebase/skins/dhtmlxwindows_dhx_blue.css">
</HTML>

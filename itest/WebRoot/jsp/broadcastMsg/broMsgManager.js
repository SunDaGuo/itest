	Date.prototype.format = function(format){
	    var o ={
	        "M+" : this.getMonth()+1, 
	        "d+" : this.getDate(),  
	        "h+" : this.getHours(),  
	        "m+" : this.getMinutes(), 
	        "s+" : this.getSeconds(),
	        "q+" : Math.floor((this.getMonth()+3)/3),  
	        "S" : this.getMilliseconds()
	    }
	    if(/(y+)/.test(format)){
	    	format=format.replace(RegExp.$1,(this.getFullYear()+"").substr(4 - RegExp.$1.length));
	    }
	    for(var k in o){
	    	if(new RegExp("("+ k +")").test(format)){
	    		format = format.replace(RegExp.$1,RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
	    	}
	    }
	    return format;
	};
	var oEditor ;
	function reSetStDate(even){
		if(even.checked==true){
			$("startDateInit").value=$("startDate").value;
			$("startDate").value=new Date().format('yyyy-MM-dd');
		}else{
			$("startDate").value=$("startDateInit").value;
		}
	}
	function viewDetl(){
		addUpInit("view");
	}
	
	var currCmd="";
	function doOnChg(val){
		if(currCmd=="view"){
			return;
		}
		if(val=="1"){
			$("recpiUserIdTr").style.display="";
			$("mailFlg").disabled=false;
		}else{
			$("recpiUserIdTr").style.display="none";
			$("mailFlg").checked=false;
			$("mailFlg").disabled=true;
		}
		adjustTable('createTable');
	}
	function findExe(){
		var url =conextPath+"/msgManager/commonMsgAction!loadMsg.action?dto.isAjax=true&dto.pageSize=" + pageSize;
		var ajaxRest = postSub(url,"findForm");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
			return;
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			$("listStr").value=ajaxRest;
			colTypeReset();
			initGrid(pmGrid,"listStr");	
	   		loadLink();
	   		fW_ch.setModal(false);
	   		fW_ch.hide();
   		}
	}
	function delExe(){
		var url=conextPath+"/msgManager/commonMsgAction!delBroMsg.action?dto.isAjax=true&dto.broMsg.logicId="+pmGrid.getSelectedId();
		var ajaxRest = postSub(url,"");
		if(ajaxRest=="failed"){
			hintMsg("删除时发生错误");
		}else{
			pmGrid.deleteRow(pmGrid.getSelectedId());
			mW_ch.setModal(false);
			mW_ch.hide();
		}
		
	}
	function findInit(){
		fW_ch = initW_ch(fW_ch,  "findDiv", true,460, 150);
		fW_ch.bringToTop();
		fW_ch.setModal(true);
		fW_ch.setText("查询");
		$("msgTitleF").focus();
	}
	String.prototype.replaceAll  = function(oldText,newText){   
	    return this.replace(new RegExp(oldText,'g'),newText);   
	};
	function addUpSub(){
		if(subChk()){
			var url =conextPath+"/msgManager/commonMsgAction!publisgMsg.action?dto.isAjax=true";
			var andMailF="0";
			if($("mailFlg").checked==true){
				$("startDate").value=new Date().format('yyyy-MM-dd');
				andMailF="1";
			}
			var ajaxRest = postSub(url,"createForm");
			if(ajaxRest=="failed"){
				hintMsg("操作失败");
				return;
			}else if(ajaxRest.indexOf("^") != -1){
				eraseAttach();
				var rowId=$("logicId").value;
				var sendDate = $("sendDate").value;
				var authName = $("sendName").value;
				if(rowId==""){
					rowId=ajaxRest.split("^")[1];
					sendDate=new Date().format('yyyy-MM-dd');
					authName=myName;
				}
				var msgTypeName =$("msgType").options[$("msgType").selectedIndex].text;
				var rowData = "0,,"+$("msgTitle").value+","+msgTypeName+","+calcStatus()+",";
				rowData+=authName+","+sendDate+","+$("startDate").value+","+$("overdueDate").value+","+ajaxRest.split("^")[2];
				if($("logicId").value!=""){
					pmGrid.deleteRow(rowId);
				}
				colTypeReset();
				pmGrid.addRow2(rowId,rowData,0);
				setRowNum(pmGrid);
				loadLink();
				cW_ch.setModal(false);
				cW_ch.hide();
				if(typeof(mypmCalendar_ch)!='undefined')
			 		mypmCalendar_ch.hide();
			}else{
				hintMsg("操作失败");
				return;
			}
		}
	}
	function calcStatus(){
		var startDate = $("startDate").value;
		var odDate = $("overdueDate").value;
		var now= (new Date()).format('yyyy-MM-dd');
		startDate = startDate.replace(/-/g, "/");
		now = now.replace(/-/g, "/");
		odDate = odDate.replace(/-/g, "/");
		startDate = new Date(startDate);
		odDate = new Date(odDate);
		now = new Date(now);
		if((startDate.getTime() - now.getTime())>0){
			return "未生效";
		}else if((odDate.getTime() - now.getTime())<=0){
			return "己失效";
		}else if((odDate.getTime() - now.getTime())>=0&&(now.getTime()-startDate.getTime())>=0){
			return "己生效";
		}
	}

	function subChk(){
		$("content").value=oEditor.GetXHTML().replace(/\s+$|^\s+/g,"");
		if(isAllNull($("msgTitle").value)){
			hintMsg("请填写标题");
			$("msgTitle").focus();
			return false;
		}else if(isAllNull($("startDate").value)){
			hintMsg("请填写生效日期");
			return false;		
		}else if(isAllNull($("overdueDate").value)){
			hintMsg("请填写过期日期");
			return false;		
		}else if($("msgType").value=="-1"){
			hintMsg("请选择类型");
			return false;		
		}else if(isAllNull($("content").value)||$("content").value=="<br>"){
			hintMsg("请填写内容");
			oEditor.Focus();
			return false;		
		}else if($("msgType").value=="1"&&$("recpiUserId").value==""){
			hintMsg("请选择接收人");
			return false;		
		}
		var startDate = $("startDate").value;
		var odDate = $("overdueDate").value;
		startDate = startDate.replace(/-/g, "/");
		odDate = odDate.replace(/-/g, "/");
		odDate = new Date(odDate);
		startDate = new Date(startDate);
		if((startDate.getTime() - odDate.getTime())>=0){
			hintMsg("失效日期必须在生效日期之后");
			return false;				 
		}
		for(var i=1; i<31;i++){
			$("content").value = $("content").value.trim("<br>");
			$("content").value = $("content").value.trim("&nbsp;");	
			$("content").value = $("content").value.replace(/\s+$|^\s+/g,"");		
		}
		if(checkIsOverLong($("content").value,1000)){
			hintMsg("内容不能超过1000个字符");
			return false;			
		}
		return true;
	}
	function loadFCK(){
		if(typeof oEditor != "undefined"){
			if($("logicId").value ==""){
				oEditor.SetData("") ;
				return;
			}else{
				oEditor.SetData($("initContent").value);
				return;
			}
		}
		importFckJs();
    	var pmEditor = new FCKeditor('content') ;
    	pmEditor.BasePath = conextPath+"/pmEditor/" ;
    	pmEditor.Height = 250;
    	pmEditor.ToolbarSet = "Basic" ; 
    	pmEditor.ToolbarStartExpanded=false;
    	pmEditor.ReplaceTextarea();
	}
	function FCKeditor_OnComplete( editorInstance ){
		oEditor = FCKeditorAPI.GetInstance('content') ;
		if($("logicId").value == ""){
			oEditor.SetData("") ;
			return;
		}else{
			oEditor.SetData($("initContent").value);
			return;
		}
	}

	pmBar.attachEvent("onValueChange", function(id, pageNo) {
		pageAction(pageNo, pageSize);
		return;
	});
	pmBar.attachEvent("onEnter", function(id, value) {
		if(!isDigit(value, false)){
			pmBar.setValue("page", pageNo);
			return;
		}
		var pageNoTemp = parseInt(value);
		if(pageNoTemp < 1){
			pageNoTemp = 1;
		}else if(pageNoTemp > pageCount){
			pageNoTemp = pageCount;
		}
		pageAction(pageNoTemp, pageSize);
		return;
	});
	pmBar.attachEvent("onClick", function(id) {
			if(id == "first"){
			pageNo = 1;
			pageAction(pageNo, pageSize);
		}else if(id == "last"){
			pageNo = pageCount;
			pageAction(pageNo, pageSize);
		}else if(id == "next"){
			pageNo = pageNo +1
			pageAction(pageNo, pageSize);
		}else if(id == "pervious"){
			pageNo = pageNo -1
			pageAction(pageNo, pageSize);
		}else if(id == "id1" || id == "id2" || id == "id3" || id == "id4" || id == "id5"){
			var pageSizeTemp = parseInt(pmBar.getListOptionText("pageP", id));
			if(pageSize==pageSizeTemp){
				return;
			}
			pageSize = pageSizeTemp;
			pageAction(pageNo, pageSize);
		}
	});
	pmBar.attachEvent("onClick", function(id) {
		if(id == "find"){
			findInit();
		}else if(id == "new"){
			addUpInit("add");
		}else if(id == 'update'){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请选择要修改的记录");
				return;
			}
			addUpInit("update");
		}else if(id == 'delete'){
			if(pmGrid.getSelectedId()==null){
				hintMsg("请选择要删除的记录");
				return;
			}
			cfDialog("delExe","您确定删除选择的记录?",false);
		}
	});
	function pageAction(pageNo, pSize){
		if(pageNo>pageCount&&opreType !="repeFind"){
			pmBar.setValue("page", pageNo);
			return ;
		}
		var url =conextPath+"/msgManager/commonMsgAction!loadMsg.action?dto.isAjax=true&dto.pageNo="+ pageNo +"&dto.pageSize=" + pageSize;
		var ajaxRest = postSub(url,"findForm");
		if(ajaxRest=="failed"){
			hintMsg("加载数据发生错误");
			return;
		}
		var userJson = ajaxRest.split("$");
		if(userJson[1] == ""){
			hintMsg("没查到相关记录");
			return;
		}else{
			$("listStr").value=ajaxRest;
			colTypeReset();
			initGrid(pmGrid,"listStr");	
	   		loadLink();
   		}
   		return;
	}
	function initCufmsW(){
		cufmsW = new dhtmlXWindows();
		cufmsW.enableAutoViewport(true);
		cufmsW.setViewport(50, 50, 100, 100);
		cufmsW.setImagePath(conextPath+"/dhtmlx/windows/codebase/imgs/");
		return cufmsW;
	}
	function initW_ch(obj, divId, mode, w, h,wId){
		importWinJs();
		if((typeof obj != "undefined")&& !obj.isHidden()){
			obj.setDimension(w, h);
			return obj;
		}else if((typeof obj != "undefined") && obj.isHidden()){
			obj.show();
			obj.bringToTop();
		}else{
			if(typeof cufmsW == "undefined"){
				cufmsW = initCufmsW();
			}
			if(typeof wId != "undefined" && wId!=""){
				obj = cufmsW.createWindow(wId, 110, 110, w, h);
			}else{
				obj = cufmsW.createWindow(divId, 110, 110, w, h);
			}
			if(divId != null&&divId !=""){
				obj.attachObject(divId, false);
			}
			hiddenB(obj, mode);
		}
		obj.centerOnScreen();
		obj.setModal(mode);
		return obj;
	}
	function addUpInit(cmd){
		initFileBk="";
		fileChkMsg="";
		initImg="";
		newUpImg="";
		currCmd = cmd;
		$('createForm').reset();
		$("currUpFile").value="";
		$("initContent").value="";
		$("logicId").value = "";
		$("recpiUserIdTr").style.display="none";
		$("currAttach").style.display="none";
		$('upStatusBar').innerHTML="";
		$("insertImg").style.display="";
		$("mailFlg").disabled=false;
		$("mailFlg").checked=false;
		loadStatus="loadIng";
		upVarReset();
		if(cmd=="update"&&pmGrid.getSelectedId()==null){
			hintMsg("请选择要修改的记录");
			return;
		}else if(cmd=="update"&&pmGrid.cells(pmGrid.getSelectedId(),4).getValue()=="己生效"){
			hintMsg("己生效不许修改");
			return;		
		}else if(cmd=="update"||cmd=="view"){
			var url =conextPath+"/msgManager/commonMsgAction!upinit.action?dto.isAjax=true&dto.broMsg.logicId="+pmGrid.getSelectedId();
			if(cmd=="view"){
				url =conextPath+"/msgManager/commonMsgAction!viewDetal.action?dto.isAjax=true&dto.broMsg.logicId="+pmGrid.getSelectedId();
				url+="&dto.isView=1";
				$("insertImg").style.display="none";
			}
			var ajaxRest = postSub(url,"");
			if(ajaxRest=="failed"){
				hintMsg("加载数据发生错误");
				return;
			}else if(ajaxRest.indexOf("failed^")>=0){
				hintMsg(ajaxRest.split("^")[1]);
				return;			
			}
			setUpInfo(ajaxRest);
		}
		if(navigator.userAgent.indexOf("Chrome")>0)
			cW_ch=initW_ch(cW_ch,  "createDiv", true,730, 470);
		else
			cW_ch=initW_ch(cW_ch,  "createDiv", true,730, 450);
		cW_ch.button("close").attachEvent("onClick", function(){
			eraseAttach('eraseAllImg');
			if(typeof(mypmCalendar_ch)!='undefined')
			 mypmCalendar_ch.hide();
			cW_ch.setModal(false);
			cW_ch.hide();
		});
		loadFCK();
		if(cmd=="view"){
			$("saveBtn").style.display="none";
			$("currUpFile").style.display="none";
			cW_ch.setText("明细");
		}else{
			$("saveBtn").style.display="";
			$("saveBtn").style.display="";
			$("currUpFile").style.display="";
			if(cmd=="update"){
				cW_ch.setText("修改");
			}else{
				cW_ch.setText("新增");
			}
		}
		adjustTable('createTable');
		cW_ch.bringToTop();
		cW_ch.setModal(true);
		$("msgTitle").focus();
	}
	function setUpInfo(updInfo){
		var updInfos = updInfo.split("^");
		for(var i=0; i < updInfos.length; i++){
			var currInfo = updInfos[i].split("=");
			if(currInfo[1] != "null"){
				var valueStr = "";
				currInfo[1] =recovJsonKeyWord(currInfo[1],'notRepComma');
				valueStr = "$('"+currInfo[0]+"').value = currInfo[1]";
				eval(valueStr);
				if(currInfo[0]=="content"){
					$("initContent").value=(currInfo[1]);
					initImg = getAttaInRichHtm(currInfo[1]);
				}else if(currInfo[0]=="mailFlg"&&currInfo[1]=="true"){
					$("mailFlg").checked=true;
				}else if(currInfo[0]=="msgType"&&currInfo[1]=="1"){
					$("recpiUserIdTr").style.display="";
				}else if(currInfo[0]=="attachUrl"&&currInfo[1]!=""){
					$("currAttach").style.display="";
					$("currAttach").title="附件";
					//$("currAttach").title=currInfo[1].substring(currInfo[1].indexOf("_")+1);
				}
			}
		}
	}
